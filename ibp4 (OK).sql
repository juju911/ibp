-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  jeu. 27 fév. 2020 à 11:11
-- Version du serveur :  5.7.19
-- Version de PHP :  7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `ibp4`
--

-- --------------------------------------------------------

--
-- Structure de la table `abouts`
--

DROP TABLE IF EXISTS `abouts`;
CREATE TABLE IF NOT EXISTS `abouts` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `titre` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `titre_en` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `description_en` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `abouts`
--

INSERT INTO `abouts` (`id`, `titre`, `titre_en`, `description`, `description_en`, `image`, `created_at`, `updated_at`) VALUES
(1, 'L\'ENTREPRISE', 'COMPANY', '<p>IBP est une&nbsp;<strong>entreprise de conseil et d’accompagnement en stratégie</strong>, issu d’un constat des deux fondateurs, qui ont décidé de mettre leurs expériences au service des autres. IBP est né pour répondre aux nouveaux besoins des organisations (grandes et petites entreprises, écosystèmes innovants, administrations et organismes du secteur public, associations…) qui font face à des défis de plus en plus complexes, dans un contexte d’incertitude accrue.&nbsp;</p><p>S’adapter à un monde connecté, globalisé, accéléré, requiert de nouvelles approches. Pour réfléchir, décider et agir efficacement, l’expertise pure ne suffit plus : il devient nécessaire de considérer les interrelations de plusieurs secteurs d’activités, de plusieurs fonctions, de plusieurs cultures.&nbsp;Pour chaque situation, IBP identifie avec vous les experts et les méthodes les mieux à même de vous aider à concevoir et mettre en œuvre votre stratégie.</p>', '<p>IBP is a strategy consulting and support company, based on an observation by the two founders, who decided to put their experiences at the service of others. IBP was born to respond to the new needs of organizations (large and small businesses, innovative ecosystems, public sector administrations and organizations, associations, etc.) which are facing increasingly complex challenges, in a context of increased uncertainty.\r\n\r\n&nbsp;\r\n</p><p>\r\nAdapting to a connected, globalized, accelerated world requires new approaches. To reflect, decide and act effectively, pure expertise is no longer enough: it becomes necessary to consider the interrelations of several activity sectors, several functions, several cultures. For each situation, IBP identifies with you the experts and methods best suited to help you design and implement your strategy.</p>', 'images/actualite/animiertes-gif-von-online-umwandeln-de (1).gif', '2020-02-25 14:54:25', '2020-02-26 12:02:18');

-- --------------------------------------------------------

--
-- Structure de la table `actualites`
--

DROP TABLE IF EXISTS `actualites`;
CREATE TABLE IF NOT EXISTS `actualites` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title_en` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `description_en` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `actualites`
--

INSERT INTO `actualites` (`id`, `title`, `title_en`, `description`, `description_en`, `image`, `created_at`, `updated_at`) VALUES
(1, 'ARTICLE SUR LE CABINET DE CONSEILS', 'ARTICLE ON THE ADVICE CABINET', '<p>essaie</p>', 'test', 'images/actualite/act.PNG', '2020-02-25 15:00:21', '2020-02-25 15:00:49');

-- --------------------------------------------------------

--
-- Structure de la table `admins`
--

DROP TABLE IF EXISTS `admins`;
CREATE TABLE IF NOT EXISTS `admins` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `admins`
--

INSERT INTO `admins` (`id`, `name`, `email`, `password`, `created_at`, `updated_at`) VALUES
(1, '', 'admin@ibp-holding.com', '5f4dcc3b5aa765d61d8327deb882cf99', NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `blogs`
--

DROP TABLE IF EXISTS `blogs`;
CREATE TABLE IF NOT EXISTS `blogs` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title_en` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description_en` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `content_en` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `blogs`
--

INSERT INTO `blogs` (`id`, `title`, `title_en`, `image`, `description`, `description_en`, `content`, `content_en`, `status`, `created_at`, `updated_at`) VALUES
(1, 'g', 'v', 'images/blog/act.PNG', 'gggggggggggggg', 'fffffffffffffffffffffffff', '<p>h</p>', 'i', 0, '2020-02-25 15:05:21', '2020-02-25 15:07:21');

-- --------------------------------------------------------

--
-- Structure de la table `comments`
--

DROP TABLE IF EXISTS `comments`;
CREATE TABLE IF NOT EXISTS `comments` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `articleId` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `contactfooters`
--

DROP TABLE IF EXISTS `contactfooters`;
CREATE TABLE IF NOT EXISTS `contactfooters` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `titre` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `titre_en` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `description_en` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `telephone` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `adresse` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `contactfooters`
--

INSERT INTO `contactfooters` (`id`, `titre`, `titre_en`, `description`, `description_en`, `email`, `telephone`, `adresse`, `created_at`, `updated_at`) VALUES
(1, 'L\'ENTREPR', 'compa', 'hhhhhhhhhhh', 'fffffffffffffff', 'i@i.fr', '444444444444', 'llllllllllll', '2020-02-25 15:17:09', '2020-02-25 15:17:40');

-- --------------------------------------------------------

--
-- Structure de la table `contacts`
--

DROP TABLE IF EXISTS `contacts`;
CREATE TABLE IF NOT EXISTS `contacts` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `content_en` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `contacts`
--

INSERT INTO `contacts` (`id`, `content`, `content_en`, `created_at`, `updated_at`) VALUES
(2, '<p>u</p>', 'v', '2020-02-25 15:14:12', '2020-02-25 15:14:31');

-- --------------------------------------------------------

--
-- Structure de la table `cvs`
--

DROP TABLE IF EXISTS `cvs`;
CREATE TABLE IF NOT EXISTS `cvs` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nom` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `offre` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cv` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `flags`
--

DROP TABLE IF EXISTS `flags`;
CREATE TABLE IF NOT EXISTS `flags` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `flags`
--

INSERT INTO `flags` (`id`, `status`, `created_at`, `updated_at`) VALUES
(1, 0, NULL, '2020-02-27 09:29:36');

-- --------------------------------------------------------

--
-- Structure de la table `ibpfooters`
--

DROP TABLE IF EXISTS `ibpfooters`;
CREATE TABLE IF NOT EXISTS `ibpfooters` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `titre` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `titre_en` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `description_en` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `ibpfooters`
--

INSERT INTO `ibpfooters` (`id`, `titre`, `titre_en`, `description`, `description_en`, `created_at`, `updated_at`) VALUES
(1, 'L\'ENTREPRISE', 'company', '<p><br></p><h4 style=\"font-family: \"Avenir LT Std 55 Roman\", sans-serif; line-height: 1.1; color: rgb(51, 51, 51); margin-right: 0px; margin-bottom: 0px; margin-left: 0px; font-size: 18px; text-align: justify;\"><span style=\"font-weight: 700;\">IBP présente son bilan annuel et ses perspectives de 2020</span></h4><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 14px; line-height: 26px; font-family: \"Avenir LT Std 55 Roman\", sans-serif; color: rgb(85, 85, 85); text-align: justify;\"> </p><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 14px; line-height: 26px; font-family: \"Avenir LT Std 55 Roman\", sans-serif; color: rgb(85, 85, 85); text-align: justify;\">Le Vendredi 31 Janvier 2020, le cabinet de conseil IBP (International Business Partners) a organisé une rencontre afin de présenter son Bilan annuel de 2019 et ses perspectives de 2020. Elle s’est faite en présence de tout le personnel à l’hôtel Bellecôte.</p>', '<p><br></p><h4 style=\"font-family: \" avenir=\"\" lt=\"\" std=\"\" 55=\"\" roman\",=\"\" sans-serif;=\"\" line-height:=\"\" 1.1;=\"\" color:=\"\" rgb(51,=\"\" 51,=\"\" 51);=\"\" margin-right:=\"\" 0px;=\"\" margin-bottom:=\"\" margin-left:=\"\" font-size:=\"\" 18px;=\"\" text-align:=\"\" justify;\"=\"\"><span style=\"font-weight: 700;\">IBP présente son bilan annuel et ses perspectives de 2020</span></h4><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 14px; line-height: 26px; font-family: \" avenir=\"\" lt=\"\" std=\"\" 55=\"\" roman\",=\"\" sans-serif;=\"\" color:=\"\" rgb(85,=\"\" 85,=\"\" 85);=\"\" text-align:=\"\" justify;\"=\"\">&nbsp;</p>', '2020-02-25 15:20:38', '2020-02-25 15:43:11');

-- --------------------------------------------------------

--
-- Structure de la table `infos`
--

DROP TABLE IF EXISTS `infos`;
CREATE TABLE IF NOT EXISTS `infos` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `texte` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `texte_en` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `couleur` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `infos`
--

INSERT INTO `infos` (`id`, `texte`, `texte_en`, `couleur`, `created_at`, `updated_at`) VALUES
(1, 'es', 'te', '#000000', '2020-02-25 15:45:54', '2020-02-25 15:46:11');

-- --------------------------------------------------------

--
-- Structure de la table `lms`
--

DROP TABLE IF EXISTS `lms`;
CREATE TABLE IF NOT EXISTS `lms` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nom` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tel` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `mail` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cv` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lm` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=364 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(335, '2014_10_12_000000_create_users_table', 1),
(336, '2014_10_12_100000_create_password_resets_table', 1),
(337, '2019_06_20_125314_create_admins_table', 1),
(338, '2019_06_20_145146_create_sliders_table', 1),
(339, '2019_06_21_171356_create_contacts_table', 1),
(340, '2019_06_22_001311_create_abouts_table', 1),
(341, '2019_06_23_103321_create_blogs_table', 1),
(342, '2019_06_23_171153_create_comments_table', 1),
(343, '2019_06_23_215547_create_sondages_table', 1),
(344, '2019_06_24_090355_create_projets_table', 1),
(345, '2019_06_25_063846_create_participants_table', 1),
(346, '2019_06_26_050235_create_actualites_table', 1),
(347, '2019_06_26_050409_create_services_table', 1),
(348, '2019_07_08_132837_create_offres_table', 1),
(349, '2019_07_10_051322_create_cvs_table', 1),
(350, '2020_01_16_232841_create_socials_table', 1),
(351, '2020_01_17_065329_create_partenaires_table', 1),
(352, '2020_01_22_102005_create_professionals_table', 1),
(353, '2020_01_22_140711_create_presentations_table', 1),
(354, '2020_01_23_102042_create_offrepresents_table', 1),
(355, '2020_01_24_111946_create_lms_table', 1),
(356, '2020_01_27_203012_create_toffres_table', 1),
(357, '2020_01_28_225956_create_ibpfooters_table', 1),
(358, '2020_01_29_003618_create_contactfooters_table', 1),
(359, '2020_02_13_152539_add_prenom_and_fonction_and_fbk_and_lkd_and_twitter_colums_to_partenaires_table', 1),
(360, '2020_02_13_162457_create_infos_table', 1),
(361, '2020_02_13_170455_add_detail_colums_to_offres_table', 1),
(362, '2020_02_13_181633_add_description_colums_to_partenaires_table', 1),
(363, '2020_02_25_163813_create_flags_table', 2);

-- --------------------------------------------------------

--
-- Structure de la table `offrepresents`
--

DROP TABLE IF EXISTS `offrepresents`;
CREATE TABLE IF NOT EXISTS `offrepresents` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `offres`
--

DROP TABLE IF EXISTS `offres`;
CREATE TABLE IF NOT EXISTS `offres` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `poste` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `poste_en` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `description_en` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `statut` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `detail` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `detail_en` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `offres`
--

INSERT INTO `offres` (`id`, `poste`, `poste_en`, `type`, `description`, `description_en`, `statut`, `created_at`, `updated_at`, `detail`, `detail_en`) VALUES
(1, 'testff', 'fredddd', 'stage', 'ddddddgggggggg', 'ffffffffffffffccccccccc', '0', '2020-02-25 16:02:43', '2020-02-25 16:04:49', '<p>ddddddddddddsssssssssss</p>', 'ffffffffffffffffhhhhhhhhh');

-- --------------------------------------------------------

--
-- Structure de la table `partenaires`
--

DROP TABLE IF EXISTS `partenaires`;
CREATE TABLE IF NOT EXISTS `partenaires` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `prenom` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fonction` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fbk` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lkd` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `twitter` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `description_en` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `partenaires`
--

INSERT INTO `partenaires` (`id`, `image`, `link`, `created_at`, `updated_at`, `prenom`, `fonction`, `fbk`, `lkd`, `twitter`, `description`, `description_en`) VALUES
(1, 'images/partenaire/act.PNG', 'SOUMAHORO', '2020-02-25 16:08:20', '2020-02-26 12:20:14', 'Ousmane', 'CEO & CO-FOUNDER', 'https://fr-fr.facebook.com/', 'zz', 'zz', '<p>- Conférencier international en entrepreneuriat &nbsp;et recherche de financement. &nbsp;</p><p>- Expert &nbsp;consultant en finance.</p>', '<p>- International speaker in entrepreneurship and fundraising.&nbsp;</p><p>- Expert consultant in finance.</p>');

-- --------------------------------------------------------

--
-- Structure de la table `participants`
--

DROP TABLE IF EXISTS `participants`;
CREATE TABLE IF NOT EXISTS `participants` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `question` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `answer` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `participants`
--

INSERT INTO `participants` (`id`, `email`, `question`, `answer`, `created_at`, `updated_at`) VALUES
(1, 'nickdev@gmail.com', '1', 'pour', '2020-02-27 09:13:20', '2020-02-27 09:13:20'),
(2, 'admin@admin.com', '1', 'pour', '2020-02-27 09:19:33', '2020-02-27 09:19:33'),
(3, 'kktjunior911@gmail.com', '1', 'pour', '2020-02-27 09:27:56', '2020-02-27 09:27:56'),
(4, 'adminibp@ibp.com', '1', 'pour', '2020-02-27 09:29:09', '2020-02-27 09:29:09'),
(5, 'f@f.fr', '1', 'pour', '2020-02-27 09:29:55', '2020-02-27 09:29:55'),
(6, 'd@p.com', '1', 'contre', '2020-02-27 09:38:57', '2020-02-27 09:38:57'),
(7, 'r@o.com', '1', 'pour', '2020-02-27 09:40:42', '2020-02-27 09:40:42'),
(8, 'e@e.fr', '1', 'contre', '2020-02-27 09:52:26', '2020-02-27 09:52:26'),
(9, 'es@f.com', '1', 'pour', '2020-02-27 09:53:10', '2020-02-27 09:53:10');

-- --------------------------------------------------------

--
-- Structure de la table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `presentations`
--

DROP TABLE IF EXISTS `presentations`;
CREATE TABLE IF NOT EXISTS `presentations` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `titre` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `presentations`
--

INSERT INTO `presentations` (`id`, `titre`, `created_at`, `updated_at`) VALUES
(1, 'testhhhhhh', '2020-02-25 16:10:39', '2020-02-25 16:13:51');

-- --------------------------------------------------------

--
-- Structure de la table `professionals`
--

DROP TABLE IF EXISTS `professionals`;
CREATE TABLE IF NOT EXISTS `professionals` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `titre` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `titre_en` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `description_en` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `professionals`
--

INSERT INTO `professionals` (`id`, `image`, `titre`, `titre_en`, `description`, `description_en`, `created_at`, `updated_at`) VALUES
(1, 'images/professional/act.PNG', 'tester une', 'testgggggggggggg', '<p>bbbbbbbbb</p>', 'xxxxx', '2020-02-25 16:21:02', '2020-02-25 16:21:24');

-- --------------------------------------------------------

--
-- Structure de la table `projets`
--

DROP TABLE IF EXISTS `projets`;
CREATE TABLE IF NOT EXISTS `projets` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `categorie` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `services`
--

DROP TABLE IF EXISTS `services`;
CREATE TABLE IF NOT EXISTS `services` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title_en` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contenue` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `contenue_en` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `services`
--

INSERT INTO `services` (`id`, `title`, `title_en`, `image`, `contenue`, `contenue_en`, `created_at`, `updated_at`) VALUES
(1, 'fffffffffffffffffffffff', 'llllllll', 'images/services/act.PNG', '<p>rrrrrr</p>', 'ffffffff', '2020-02-25 16:24:37', '2020-02-25 16:24:58');

-- --------------------------------------------------------

--
-- Structure de la table `sliders`
--

DROP TABLE IF EXISTS `sliders`;
CREATE TABLE IF NOT EXISTS `sliders` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `titre` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `socials`
--

DROP TABLE IF EXISTS `socials`;
CREATE TABLE IF NOT EXISTS `socials` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `logo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `sondages`
--

DROP TABLE IF EXISTS `sondages`;
CREATE TABLE IF NOT EXISTS `sondages` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `question` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `question_en` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pour` int(11) NOT NULL DEFAULT '0',
  `contre` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `sondages`
--

INSERT INTO `sondages` (`id`, `question`, `question_en`, `pour`, `contre`, `created_at`, `updated_at`) VALUES
(1, 'vvvvvvvvv', 'xxxxxxxxxxxxx', 7, 2, '2020-02-25 16:30:52', '2020-02-25 16:31:05');

-- --------------------------------------------------------

--
-- Structure de la table `toffres`
--

DROP TABLE IF EXISTS `toffres`;
CREATE TABLE IF NOT EXISTS `toffres` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title_en` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description_en` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
