<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContactfootersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contactfooters', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('titre');
            $table->string('titre_en');
            $table->longText('description');
            $table->longText('description_en');
            $table->longText('email');
            $table->longText('telephone');
            $table->longText('adresse');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contactfooters');
    }
}
