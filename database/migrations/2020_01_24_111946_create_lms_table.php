<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLmsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lms', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nom');
            $table->longText('tel');
            $table->string('mail');
            $table->string('type');
            $table->string('cv');
            $table->string('lm');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lms');
    }
}
