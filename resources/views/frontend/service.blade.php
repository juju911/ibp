@extends('layouts.frontend')

@section('content')


<style>


#about {
    display: block;
    margin-top:5vw;
    margin-bottom: 10vw;
}

.subtittle h2:after {
    content: "";
    position: absolute;
    height: 2px;
    width: 80px;
    background: #222222;
    left: 0;
    bottom: -22px;
}

.subtittle h2 {
    font: 700 30px 'Avenir LT Std 55 Roman', sans-serif;
    color: #1b3768;
    text-transform: uppercase;
    position: relative;
    margin-bottom: 20px;
}

.subtittle{
    padding-bottom: 30px;
}

.justifie{
    text-align:justify;
}

.large{
    width:100%;
    height:auto;
}  

.marge{
    margin-bottom:2vw;
}
</style>

    <!-- Banner area -->
  

    <!-- Building Construction Area -->
    <section aria-label="Detail" id="about">
                      <div class="container">

                           @foreach($services as $s)
                        <div class="row marge">


                         @if (($s->id % 2) == 0)
                          <!-- heading text --> 
                         
                            <div class="col-md-6 ">
                                @if (session('status') == 1)
                                                                
                                    <div class="subtittle">
                                        <h2> {{$s->title}}</h2>
                                    </div>
                                    <p class="justifie">
                                        {!!$s->contenue!!}
                                    </p>
                                @else
                                    
                                    <div class="subtittle">
                                        <h2> {{$s->title_en}}</h2>
                                    </div>
                                    <p class="justifie">
                                        {!!$s->contenue_en!!}
                                    </p>
                                @endif
                                
                                
                            </div>
                           <!-- heading text end --> 
                          
                            <div class="col-md-6">
                                <img class="large" src="{{URL::to($s->image)}}" alt="img-responsive">
                            </div>
                           

                         @else
                       
                            <div class="col-md-6 ">
                                <img class="large" src="{{URL::to($s->image)}}" alt="img-responsive">                             
                            </div>
                           <!-- heading text end --> 
                          
                            <div class="col-md-6">

                                @if (session('status') == 1)
                                                            
                                    <div class="subtittle">
                                        <h2> {{$s->title}}</h2>
                                    </div>
                                    <p class="justifie">
                                        {!!$s->contenue!!}
                                    </p>
                                @else
                                    
                                    <div class="subtittle">
                                        <h2> {{$s->title_en}}</h2>
                                    </div>
                                    <p class="justifie">
                                        {!!$s->contenue_en!!}
                                    </p>
                                @endif
                                
                                
                            </div>
                         
                        @endif
                          
                        </div>
                         @endforeach
                      </div>
                    </section>

    <!-- End Our Services Area -->
@endsection
