@extends('layouts.frontend')

@section('content')

<style>

.color{
   color: white;
}

.offre{
   display:block; 
   text-transform:uppercase; 
   margin-left:50%; 
   margin-top:25px
}

.offre2{
    color:#fff; 
    font-size:1vw; 
    line-height:normal; 
    margin-top:5px
}

.proj{
    display:block; 
    text-transform:uppercase; 
    margin-right:50%
}

.proj1{
    color:#444; 
    width:100%; 
    margin-bottom:50px;
    margin-top:50px; 
    display:block; 
    font-size:2vw; 
    font-family: 'HP Simplified Light'; 
    border-bottom:1px solid #ccc; 
    padding-bottom:20px;
}

.proj2{
    color:#444; 
    width:100%; 
    margin-bottom:50px;
    margin-top:50px; 
    display:block; 
    font-size:1.5em; 
    font-family: 'HP Simplified Light'; 
    border-bottom:1px solid #ccc; 
    padding-bottom:20px;
}

.off{
    width:90%; 
    word-wrap: break-word; 
    margin-left:auto; 
    margin-right:auto
}

.off_post{
    font-family:'HP Simplified Bold'; 
    font-size:30px; 
    color:#fd6604
}

.off_post2{
   font-family:'HP Simplified Light'; 
   margin-left:20px; 
   font-size:1.2em; 
   margin-top:3px; 
   width:70%;
   word-wrap: break-word;
}

.taille{
    font-size:20px
}

hr{
    width:50%;
}

.hr{
    width:50%; 
    height: 3px; 
    background-color: #1B3768;
}

.div_post{
    width:100%; 
    display:block;  
    float:left; 
    border-bottom:1px solid #ccc; 
    padding-bottom:40px; 
    margin-bottom:40px;
}

.div_post_post{
    width:70%; 
    display:block;  
    float:left;
}

.date_offre{
    font-family:'HP Simplified Regular';  
    color:#444; 
    font-size:12px
}

.type_offre{
    font-family:'HP Simplified Bold'; 
    font-size:20px; 
    color:#444
}

.div_type{
    width:30%; 
    display:block;  
    float:right;  
}

</style>

<!-- header  -->

    <!-- Header For Mobile -->
    <section class="banner_areaa hidden-lg hidden-md" data-stellar-background-ratio="0.5">
      
      @foreach($toffres as $toffre)
    <div>
        <h2>
            @if (session('status') == 1)
                                    
                {{$toffre->title}}
    
            @else
                {{$toffre->title_en}}
            @endif
        
        </h2>
        <span class="spa">
            @if (session('status') == 1)
                                    
                {{$toffre->description}}
    
            @else
                {{$toffre->description_en}}
            @endif
            
        
        
        </span>
    </div>
        @endforeach

      <hr>

        <div align="center">
            <a href="" class="color">
                <img src="../../images/projets/online.png" width="10%">
                @if (session('status') == 1)
                                    
                    OFFRE D'EMPLOI
        
                @else
                    JOB OFFER
                @endif  
            </a>

            <a href="" class="btn btn-outline-primary btn-sm color" data-toggle="modal" data-target="#largeModale">
                <img src="../../images/projets/cv.png" width="10%"> 
                @if (session('status') == 1)
                                    
                    CANDIDATURES SPONTANEES
        
                @else
                    
                    UNSOLICITED APPLICATIONS
                @endif   
            </a>

        </div>

         <div class="modal fade" id="largeModale" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true" >
                        <div class="modal-dialog modal-md" role="document">
                            <form action="{{url('/save-lm')}}" method="post" enctype="multipart/form-data">
                                @csrf
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="largeModalLabel">
                                            @if (session('status') == 1)
                                        
                                                CANDIDATURES SPONTANEES
                                    
                                            @else
                                                
                                                UNSOLICITED APPLICATIONS
                                            @endif
                                        </h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                       
                                        <div class="row">
                                            <div class="col-md-12 ">
                                                <div class="form-group">
                                                    <label for="question" class="control-label mb-1">
                                                    @if (session('status') == 1)
                                        
                                                        Votre Nom
                                            
                                                    @else
                                                        
                                                        Your Name
                                                    @endif
                                                    </label>
                                                    <input type="text" id="question" name="name" class="form-control" required placeholder="Votre Nom">
                                                </div>
                                                <div class="form-group">
                                                    <label for="question" class="control-label mb-1">
                                                    @if (session('status') == 1)
                                        
                                                        Numéro de téléphone
                                            
                                                    @else
                                                        Phone number
                                                    @endif
                                                    </label>
                                                    <input type="text" id="question" name="tel" class="form-control" required placeholder="Votre Téléphone">
                                                </div>
                                                <div class="form-group">
                                                    <label for="question" class="control-label mb-1">Mail</label>
                                                    <input type="mail" id="question" name="mail" class="form-control" required placeholder="Votre mail">
                                                </div>
                                                <div class="control-group">
                                                <label class="control-label" for="question">
                                                @if (session('status') == 1)
                                        
                                                    Type de contrat
                                        
                                                @else
                                                    Type of Contract
                                                @endif
                                                </label>
                                                <div class="controls">
                                                    <select type="text" name="type" id="question"  class="form-control">
                                                        <option value=" ">
                                                        @if (session('status') == 1)
                                        
                                                            Selectionez le type de contrat
                                                
                                                        @else
                                                            Select the type of contract
                                                        @endif
                                                        </option>
                                                        <option value="stage">
                                                            @if (session('status') == 1)
                                        
                                                               Stage
                                                    
                                                            @else
                                                                Traineeship
                                                            @endif
                                                        </option>
                                                        <option value="CDD">CDD</option>
                                                        <option value="CDI">CDI</option>
                                                    </select>
                                                </div>
                                            </div>
                                                <div class="form-group">
                                                    <label for="question" class="control-label mb-1"> @if (session('status') == 1)
                                        
                                                        Votre Cv
                                            
                                                    @else
                                                        Your Cv
                                                    @endif
                                                    </label>
                                                    <input type="file" id="cv" name="cv" class="form-control" required placeholder="Votre Cv">
                                                   
                                                </div>
                                                <div class="form-group">
                                                    <label for="question" class="control-label mb-1">
                                                    @if (session('status') == 1)
                                        
                                                        Votre lettre de motivation
                                            
                                                    @else
                                                        Your cover letter
                                                    @endif
                                                    </label>
                                                    <input type="file" id="lm" name="lm" class="form-control" required placeholder="Votre LM">
                                                  
                                                </div>
                                               
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                        @if (session('status') == 1)
                                                            
                                            Annuler
                    
                                        @else
                                            
                                            Cancel
                                        @endif
                                        </button>
                                        <button type="submit" class="btn btn-primary">
                                        @if (session('status') == 1)
                                                            
                                            Valider
                    
                                        @else
                                            
                                            Validate
                                        @endif
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

            </div>
    
    </section>
    
    <!-- Header For Mobile -->

    <!-- Header for normal -->
    <section class="banner_areaa hidden-xs hidden-sm" data-stellar-background-ratio="0.5">
    @foreach($toffres as $toffre)
    <div>
        <h2>
        @if (session('status') == 1)
                                                            
            {{$toffre->title}}

        @else
            
            {{$toffre->title_en}}
        @endif
        
        </h2>
        <span class="spa">
        @if (session('status') == 1)
                                                            
            {{$toffre->description}}

        @else
            
            {{$toffre->description_en}}
        @endif
        
        </span>
    </div>
        @endforeach
        <hr>

        <span align="center">

        <div class="row ">
            <div class="col-md-6">
                <div class="offre" align="center">
                    <a href="#">
                        <p>
                            <img src="../../images/projets/online.png" width="10%">
                        </p>
                        <p  class="offre2" align="center">

                            @if (session('status') == 1)
                                    
                                OFFRES<br> D'EMPLOI
                    
                            @else
                                JOB <br> OFFER
                            @endif 
                           
                        </p>
                    </a>
                </div>
            </div>

<!-- candidature -->
            <div class="col-md-6">
           
        <div class="proj" align="center">
            <a href="" class="btn-lg" data-toggle="modal" data-target="#largeModalc">
                <p>
                    <img src="../../images/projets/cv.png" width="10%">
                </p>
                <p class="offre2" align="center">
                     @if (session('status') == 1)
                                    
                        CANDIDATURES<br> SPONTANEES
            
                    @else
                        
                        UNSOLICITED<br> APPLICATIONS
                    @endif 
              
                </p>
            </a>
        </div>
        <div class="modal fade" id="largeModalc" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true" >
                        <div class="modal-dialog modal-md" role="document">
                            <form action="{{url('/save-lm')}}" method="post" enctype="multipart/form-data">
                                @csrf
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="largeModalLabel">
                                        @if (session('status') == 1)
                                        
                                            CANDIDATURES SPONTANEES
                                
                                        @else
                                            
                                            UNSOLICITED APPLICATIONS
                                        @endif
                                        </h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                       
                                        <div class="row">
                                            <div class="col-md-12 ">
                                                <div class="form-group">
                                                    <label for="question" class="control-label mb-1">
                                                    @if (session('status') == 1)
                                        
                                                        Votre Nom
                                            
                                                    @else
                                                        
                                                        Your Name
                                                    @endif
                                                    </label>
                                                    <input type="text" id="question" name="name" class="form-control" required placeholder="Votre Nom">
                                                </div>
                                                <div class="form-group">
                                                    <label for="question" class="control-label mb-1">
                                                    @if (session('status') == 1)
                                        
                                                        Numéro de téléphone
                                            
                                                    @else
                                                        Phone number
                                                    @endif
                                                    </label>
                                                    <input type="text" id="question" name="tel" class="form-control" required placeholder="Votre Téléphone">
                                                </div>
                                                <div class="form-group">
                                                    <label for="question" class="control-label mb-1">Mail</label>
                                                    <input type="mail" id="question" name="mail" class="form-control" required placeholder="Votre mail">
                                                </div>
                                                <div class="control-group">
                                                <label class="control-label" for="question">
                                                @if (session('status') == 1)
                                        
                                                    Type de contrat
                                        
                                                @else
                                                    Type of Contract
                                                @endif
                                                </label>
                                                <div class="controls">
                                                    <select type="text" name="type" id="question"  class="form-control">
                                                        <option value=" ">
                                                        @if (session('status') == 1)
                                        
                                                            Selectionez le type de contrat
                                                
                                                        @else
                                                            Select the type of contract
                                                        @endif
                                                        </option>
                                                        <option value="stage">
                                                        @if (session('status') == 1)
                                        
                                                            Stage
                                                
                                                        @else
                                                            Traineeship
                                                        @endif
                                                        </option>
                                                        <option value="CDD">CDD</option>
                                                        <option value="CDI">CDI</option>
                                                    </select>
                                                </div>
                                            </div>
                                                <div class="form-group">
                                                    <label for="question" class="control-label mb-1">
                                                    @if (session('status') == 1)
                                        
                                                        Votre Cv
                                            
                                                    @else
                                                        Your Cv
                                                    @endif
                                                    </label>
                                                    <input type="file" id="cv" name="cv" class="form-control" required placeholder="Votre Cv">
                                                   
                                                </div>
                                                <div class="form-group">
                                                    <label for="question" class="control-label mb-1">
                                                    @if (session('status') == 1)
                                        
                                                        Votre lettre de motivation
                                            
                                                    @else
                                                        Your cover letter
                                                    @endif
                                                    </label>
                                                    <input type="file" id="lm" name="lm" class="form-control" required placeholder="Votre LM">
                                                  
                                                </div>
                                               
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                        @if (session('status') == 1)
                                                            
                                            Annuler
                    
                                        @else
                                            
                                            Cancel
                                        @endif
                                        </button>
                                        <button type="submit" class="btn btn-primary">
                                        @if (session('status') == 1)
                                                            
                                            Valider
                    
                                        @else
                                            
                                            Validate
                                        @endif
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

            </div>
            
        </div>

      

</span>
    </section>
    <!-- End Header normal -->

<!-- End header -->


    <!-- Section normal -->
    <section>
        <div class="container hidden-xs">
        <div class="proj1" align="center"><i class="fa fa-external-link" aria-hidden="true"></i>&nbsp;
        @if (session('status') == 1)
                                                            
            Nos offres d'emploi

        @else
            
            Our job offers
        @endif
        
        </div>
        </div>
    </section>
    <section>
        <div class="row ">
            <div class="hidden-xs">
        <div class="container">
        @include('includes.validator')

          @foreach($offres as $offre)
            <div class="div_post">

                <div class="div_post_post" >
                    <p class="off_post">
                    @if (session('status') == 1)
                                                            
                        + {{$offre->poste}}

                    @else
                        
                        + {{$offre->poste_en}}
                    @endif
                     
                    </p>
                   
                    <p class="off_post2">
                    @if (session('status') == 1)
                                                            
                        {{$offre->description}}
                    @else
                        
                        {{$offre->description_en}}
                    @endif
                        
                              
                    </p>

                </div>

                <div class="div_type" align="center">

                    <p class="type_offre">
                    {{$offre->type}}
                    </p>
                    <p class="date_offre">
                    @if (session('status') == 1)
                                                            
                        Publiée le: {{\Carbon\Carbon::parse($offre->created_at)->format('d/m/y')}} 
                    @else
                        
                       
                        Published on:  {{\Carbon\Carbon::parse($offre->created_at)->format('d/m/y')}} 
                    @endif
                        
                    </p>
                    <p id="joblink">
                        <a href="{{url('/details-offre/'.$offre->id)}}"  class="btn btn-primary btn-xs color">
                        @if (session('status') == 1)
                                                            
                            Consulter l'offre
                        @else
                            
                            Consult the offer
                        @endif
                            
                        </a
                    ></p>

                </div>

            </div>
            
            @endforeach
        </div>

        <span class="icon-bar"></span>
        </div>
        </div>
    </section>

    <!-- end section normal -->

     <!-- Section for mobile -->
     <section>
        <div class="container hidden-lg hidden-md hidden-sm" align="center">
        <div class="proj2" ><i class="fa fa-external-link" aria-hidden="true" ></i>&nbsp;
        @if (session('status') == 1)
                                                            
            Nos offres d'emploi

        @else
            
            Our job offers
        @endif
        </div>
        </div>
    </section>
    <section>
            <div class="row hidden-lg hidden-md hidden-sm" >
            @include('includes.validator')
            @foreach($offres as $offre)
                <div class="off" align="center" >
                        <h2> 
                            <span class="off_post">
                            @if (session('status') == 1)
                                                            
                                + {{$offre->poste}}
        
                            @else
                                
                                + {{$offre->poste_en}}
                            @endif
                            </span>
                        </h2> 
                         <p class="off_post2">
                         @if (session('status') == 1)
                                                            
                            {{$offre->description}}
                        @else
                            
                            {{$offre->description_en}}
                        @endif 
                         </p>
                        <br>  <b class="taille">{{$offre->type}}</b>
                        <br>@if (session('status') == 1)
                                                            
                                Publiée le: {{\Carbon\Carbon::parse($offre->created_at)->format('d/m/y')}} 
                            @else
                                
                                
                                Published on:  {{\Carbon\Carbon::parse($offre->created_at)->format('d/m/y')}} 
                            @endif
                        <br>
                        <br> <p id="joblink">
                        <a href="{{url('/details-offre/'.$offre->id)}}"class="btn btn-primary btn-xs color">
                        @if (session('status') == 1)
                                                            
                            Consulter l'offre
                        @else
                            
                            Consult the offer
                        @endif
                        </a></p>
                </div>
                <hr class="hr">
               
            @endforeach
            </div>
    </section>

     <!--End Section for mobile -->

    
   
   
    <!-- End About Us Area -->

@endsection
