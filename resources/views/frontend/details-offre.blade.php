@extends('layouts.frontend')

@section('content')

<style>
.off_post{
    font-family:'HP Simplified Bold'; 
    font-size:20px !important; 
    color:#fd6604 !important;
}

.color{
   color: white !important;
   width:50%;
}
</style>

    <!-- Banner area -->
    <section class="banner_area hidden-xs" data-stellar-background-ratio="0.5">
        <h2><b>
            @if (session('status') == 1)
                                    
                {{$offre->poste}}

            @else
                {{$offre->poste_en}}
            @endif
        </b></h2>
        <!-- <ol class="breadcrumb">
            <li><a href="{{url('/')}}">Acceuil</a></li>
            <li><a href="#" class="active">Actualité</a></li>
        </ol> -->
    </section>

    <!-- Ecran mobile -->

    <!-- Building Construction Area -->
    <section class="building_construction_area hidden-lg hidden-md hidden-sm">
        <div class="container">
            <div class="row building_construction_row">
                <div class="col-sm-9 constructing_laft">
                    <h2>
                        @if (session('status') == 1)
                                                
                            {{$offre->poste}}

                        @else
                            {{$offre->poste_en}}
                        @endif
                    </h2>
                    <div class="col-md-12 ipsum" >
                        @if (session('status') == 1)
                                            
                            {!!$offre->detail!!}
    
                        @else
                            {!!$offre->detail_en!!}
                        @endif
                        
                        
                    </div>
                    <small style="float:right; margin-top:30px">
                        @if (session('status') == 1)
                                            
                            Publié le: {{\Carbon\Carbon::parse($offre->created_at)->format('d/m/Y')}}
    
                        @else
                            Published on: {{\Carbon\Carbon::parse($offre->created_at)->format('Y/m/d')}}
                        @endif
                    </small>
                  
                   
                       
                    <div class="col-md-6 ipsum_img" align="center">
                  
                        <img src="images/construction-2.jpg" alt="">
                    </div>
                   
                   
                </div>
                <div align="center">
                    <a href="" class="btn btn-primary color" data-toggle="modal" data-target="#largeModal" >
                        @if (session('status') == 1)
                                                
                            Postuler

                        @else
                            Apply
                        @endif
                                
                    </a>
                </div>
                <div class="col-sm-3 constructing_right">
                    <h3>
                        @if (session('status') == 1)
                                            
                            Autres offres
    
                        @else
                            Other offers
                        @endif
                    </h3>
                    <br>
                    @if(count($others)>0)
                    <ul class="painting">
                        @foreach($others as $other)
                        <li>
                            <a href="{{url('/details-offre/'.$other->id)}}" class="off_post">
                                @if (session('status') == 1)
                                            
                                    + {{$other->poste}}
            
                                @else
                                    + {{$other->poste_en}}
                                @endif
                            </a>
                        </li>
                        @endforeach
                    </ul>
                    @else
                        <p>
                            @if (session('status') == 1)
                                            
                                Pas d'autres offres
        
                            @else
                                No other offers
                            @endif
                        
                        </p>
                    @endif
                </div>
            </div>
        </div>

        <div class="modal fade" id="largeModal" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-md" role="document">
                            <form action="{{url('/save-cv')}}" method="post"                            enctype="multipart/form-data">
                                @csrf
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="largeModalLabel">
                                            @if (session('status') == 1)
                                                
                                                Nos offres
                        
                                            @else
                                                Our offers
                                            @endif
                                        </h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="card-title">
                                            <h3 class="text-center title-2">
                                                @if (session('status') == 1)
                                                    
                                                    {{$offre->poste}}
                            
                                                @else
                                                    {{$offre->poste_en}}
                                                @endif
                                            </h3>
                                        </div>
                                        <hr>
                                        <div class="row">
                                            <div class="col-md-12 ">
                                                <div class="form-group">
                                                    <label for="question" class="control-label mb-1">
                                                        @if (session('status') == 1)
                                                        
                                                            Votre Nom
                                    
                                                        @else
                                                            
                                                            Your name
                                                        @endif
                                                    
                                                    </label>
                                                    <input type="text" id="question" name="name" class="form-control" required placeholder="Votre Nom">
                                                </div>
                                                <div class="form-group">
                                                    <label for="question" class="control-label mb-1">
                                                        @if (session('status') == 1)
                                                            
                                                            Votre CV
                                    
                                                        @else
                                                            
                                                            Your CV
                                                        @endif
                                                    </label>
                                                    <input type="file" id="question" name="cv" class="form-control" required placeholder="Votre Cv">
                                                    <input type="hidden" name="offre" value="{{$offre->id}}">
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                            @if (session('status') == 1)
                                                            
                                                 Annuler
                        
                                            @else
                                                
                                                Cancel
                                            @endif
                                        </button>
                                        <button type="submit" class="btn btn-primary">
                                            @if (session('status') == 1)
                                                            
                                                Valider
                        
                                            @else
                                                
                                                Validate
                                            @endif
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
        </div>
    </section>
    <!-- End Building Construction Area -->

    <!-- end ecran mobile -->

      <!-- Ecran normal -->

    <!-- Building Construction Area -->
    <section class="building_construction_area hidden-xs">
        <div class="container">
            <div class="row building_construction_row">
                <div class="col-sm-9 constructing_laft">
                    <h2>
                        @if (session('status') == 1)
                                                            
                            {{$offre->poste}}
    
                        @else
                            
                            {{$offre->poste_en}}
                        @endif
                    
                    </h2>
                    <div class="col-md-12 ipsum" >
                        @if (session('status') == 1)
                                                            
                            {!!$offre->detail!!}
    
                        @else
                            
                            {!!$offre->detail_en!!}
                        @endif
                       
                        
                    </div>
                    <small style=" margin-top:30px">
                        @if (session('status') == 1)
                                            
                            Publié le: {{\Carbon\Carbon::parse($offre->created_at)->format('d/m/Y')}}
    
                        @else
                            Published on: {{\Carbon\Carbon::parse($offre->created_at)->format('Y/m/d')}}
                        @endif
                    
                    </small>
                  
                   
                   
                    <div class="col-md-6 ipsum_img" align="center">
                  
                        <img src="images/construction-2.jpg" alt="">
                        <a href="" class="btn btn-primary color" data-toggle="modal" data-target="#largeModalm" >
                            @if (session('status') == 1)
                                            
                                Postuler
        
                            @else
                                Apply
                            @endif
                                                
                        </a>
                    </div>
                   
                   
                </div>
                
               
                <div class="col-sm-3 constructing_right">
                    <h3>
                        @if (session('status') == 1)
                                            
                            Autres offres
    
                        @else
                            Other offers
                        @endif
                    </h3>
                    <br>
                    @if(count($others)>0)
                    <ul class="painting">
                        @foreach($others as $other)
                            
                        <li>
                            <a href="{{url('/details-offre/'.$other->id)}}" class="off_post">
                                @if (session('status') == 1)
                                            
                                    + {{$other->poste}}
            
                                @else
                                    + {{$other->poste_en}}
                                @endif
                            </a>
                        </li>
                        @endforeach
                    </ul>
                    @else
                        <p>
                            @if (session('status') == 1)
                                            
                                Pas d'autres offres
        
                            @else
                                No other offers
                            @endif
                        
                        </p>
                    @endif


                </div>
            </div>
        </div>

        <div class="modal fade" id="largeModalm" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-md" role="document">
                            <form action="{{url('/save-cv')}}" method="post" enctype="multipart/form-data">
                                @csrf
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="largeModalLabel">
                                            @if (session('status') == 1)
                                                
                                                Nos offres
                        
                                            @else
                                                Our offers
                                            @endif
                                        </h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="card-title">
                                            <h3 class="text-center title-2">
                                                @if (session('status') == 1)
                                                    
                                                    {{$offre->poste}}
                            
                                                @else
                                                    {{$offre->poste_en}}
                                                @endif
                                            </h3>
                                        </div>
                                        <hr>
                                        <div class="row">
                                            <div class="col-md-12 ">
                                                <div class="form-group">
                                                    <label for="question" class="control-label mb-1">
                                                        @if (session('status') == 1)
                                                        
                                                            Votre Nom
                                    
                                                        @else
                                                            Your Name
                                                        @endif
                                                    </label>
                                                    <input type="text" id="question" name="name" class="form-control" required placeholder="Votre Nom">
                                                </div>
                                                <div class="form-group">
                                                    <label for="question" class="control-label mb-1">
                                                        @if (session('status') == 1)
                                                            
                                                            Votre Cv
                                    
                                                        @else
                                                            Your Cv
                                                        @endif
                                                   </label>
                                                    <input type="file" id="question" name="cv" class="form-control" required placeholder="Votre Cv">
                                                    <input type="hidden" name="offre" value="{{$offre->id}}">
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                            @if (session('status') == 1)
                                                            
                                                Annuler
                        
                                            @else
                                                
                                                Cancel
                                            @endif
                                        </button>
                                        <button type="submit" class="btn btn-primary">
                                            @if (session('status') == 1)
                                                            
                                                Valider
                        
                                            @else
                                                
                                                Validate
                                            @endif
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
    </section>
    <!-- End Building Construction Area -->

    <!-- end ecran normal -->
@endsection
