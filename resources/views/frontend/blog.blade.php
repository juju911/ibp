@extends('layouts.frontend')

@section('content')

<style>

.date_blog{
    float:left;
}

.img_blog{
    width:360px;
    height:170px;
}

.img_content{
    overflow:hidden; 
    word-wrap: break-word; 
    height:120px 
}

</style>
    <!-- Banner area -->
    <section class="banner_area" data-stellar-background-ratio="0.5">
        <h2><b> 
            @if (session('status') == 1)
                                
               Blog

            @else
                Blog
            @endif  
        </b></h2>
        <!-- <ol class="breadcrumb">
            <li><a href="index.html">Acceuil</a></li>
            <li><a href="#" class="active">Blog </a></li>
        </ol> -->
    </section>
    <!-- End Banner area -->
    <!-- blog-2 area -->
    <section class="blog_tow_area">
        <div class="container">
            <div class="row blog_tow_row">
                @foreach($articles as $article)
                <a class="tittle" href="{{URL::to('/blog/article/'.$article->id)}}">
                    <div class="col-md-4 col-sm-6 wow bounceInDown">
                        <div class="renovation" >
                            <img src="{{URL::to($article->image)}}" alt="" class="img_blog" >
                            <div class="renovation_content img_content" >
                            @if (session('status') == 1)
                                
                                {{$article->title}}
                                <div class="date_comment">
                                    <a class="date_blog"><i class="fa fa-calendar" aria-hidden="true"></i>{{\Carbon\Carbon::parse($article->created_at)->format('d/m/Y')}}</a>
                                </div>
                 
                             @else

                                {{$article->title_en}}
                                <div class="date_comment">
                                    <a class="date_blog"><i class="fa fa-calendar" aria-hidden="true"></i>{{\Carbon\Carbon::parse($article->created_at)->format('Y/m/d')}}</a>
                                </div>

                             @endif 
                              
                            </div>
                        </div>
                    </div>
                </a>
                @endforeach
            </div>
        </div>
    </section>
    <!-- End blog-2 area -->
@endsection
