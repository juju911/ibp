@extends('layouts.frontend')

@section('content')

<style type="text/css">
    .panel-heading{
        word-wrap: break-word;
    }
</style>
    <!-- Banner area -->
    <section class="banner_area" data-stellar-background-ratio="0.5">
        <h2>
        @if (session('status') == 1)
                                                            
            Resultat Sondage
        @else
            
            Survey Result
        @endif
       </h2>
        <!-- <ol class="breadcrumb">
            <li><a href="{{url('/')}}">Acceuil</a></li>
            <li><a href="#" class="active">Sondage</a></li>
        </ol> -->
    </section>
    <!-- End Banner area -->
    @include('includes.validator')
    <!-- blog area -->
    <section class="blog_all">
        <div class="container">
            <div class="row text-center">
                <div class="col-md-12 col-xs-12">
                    <div class="panel panel-default">
                        <?php
                        $q=DB::table('sondages')->where('id',$question)->first();
                        ?>

                        <div class="panel-heading">
                        @if (session('status') == 1)
                                                            
                            {{$q->question}}
                        @else
                            
                            {{$q->question_en}}
                        @endif
                        
                        </div>

                        <div class="panel-body">
                            {!! $chart->html() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End blog area -->

    {!! Charts::scripts() !!}
    {!! $chart->script() !!}
@endsection
