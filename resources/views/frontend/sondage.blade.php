@extends('layouts.frontend')

@section('content')

<style type="text/css">
.break_text{
    word-wrap: break-word;
}

.color_p{
    color: #fff !important;
}

.date_pro{
    float:right; 
    margin-top:30px;
}


</style>

    <!-- Banner area -->
    <section class="banner_area" data-stellar-background-ratio="0.5">
        <h2><b>
        @if (session('status') == 1)
                                                            
            Sondages
        @else
            
            Surveys
        @endif
        
         </b></h2>
        <!-- <ol class="breadcrumb">
            <li><a href="{{url('/')}}">Acceuil</a></li>
            <li><a href="#" class="active">Sondage</a></li>
        </ol> -->
    </section>
    <!-- End Banner area -->

    <!-- About Us Area -->
    <section class="what_we_area row">

        <div class="container">
            @include('includes.validator')
            <div class="row construction_iner">
                @foreach($questions as $q)
                <div class="col-md-4 col-sm-6 construction wow zoomIn">
                    <div class="cns-content">
                        <i class="fa fa-question" aria-hidden="true"></i>
                        <a class="break_text">
                        @if (session('status') == 1)
                                                            
                            {{$q->question}}
                        @else
                            
                            {{$q->question_en}}
                        @endif
                        
                       </a>
                        <br>
                        <a href="" class="btn btn-primary btn-lg color_p" data-toggle="modal" data-target="#largeModal{{$q->id}}">
                        @if (session('status') == 1)
                                                            
                            Participer au sondage
                        @else
                            
                            Take the survey
                        @endif
                        
                        </a>
                    </div>
                </div>
                <div class="modal fade" id="largeModal{{$q->id}}" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-md" role="document">
                            <form action="{{url('/do-sondage')}}" method="post">
                                @csrf
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="largeModalLabel">
                                        @if (session('status') == 1)
                                                            
                                            Sondages
                                        @else
                                            
                                            Surveys
                                        @endif
                                        </h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="card-title">
                                            <h3 class="text-center title-2 break_text">
                                            @if (session('status') == 1)
                                                            
                                                {{$q->question}}
                                            @else
                                                
                                                {{$q->question_en}}
                                            @endif
                                            </h3>
                                        </div>
                                        <hr>
                                        <div class="row">
                                            <div class="col-md-12 ">
                                                <div class="form-group">
                                                    <label for="question" class="control-label mb-1">@if (session('status') == 1)
                                                            
                                                            Votre Email
                                                        @else
                                                            
                                                            Your E-mail
                                                        @endif
                                                    </label>
                                                    <input type="email" id="question" name="email" class="form-control" required placeholder="Votre Email">
                                                </div>
                                                <div class="form-group text-center">
                                                    <input type="hidden" name="question" value="{{$q->id}}">

                                                    <label for="question" class="control-label mb-1">
                                                    @if (session('status') == 1)
                                                            
                                                            OUI
                                                        @else
                                                            
                                                            YES
                                                        @endif
                                                    </label> :
                                                    <input type="radio" name="answer" id="question" required value="pour">
                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                    <label for="question" class="control-label mb-1">
                                                    @if (session('status') == 1)
                                                            
                                                            NON
                                                        @else
                                                            
                                                            NO
                                                        @endif
                                                    </label> :
                                                    <input type="radio" name="answer" id="question" required value="contre">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                        @if (session('status') == 1)
                                                            
                                            Annuler
                    
                                        @else
                                            
                                            Cancel
                                        @endif
                                        </button>
                                        <button type="submit" class="btn btn-primary">
                                        @if (session('status') == 1)
                                                            
                                            Valider
                    
                                        @else
                                            
                                            Validate
                                        @endif
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
    <!-- End About Us Area -->

@endsection
