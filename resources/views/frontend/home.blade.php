@extends('layouts.frontend')

@section('content')

<style>
.color_slide{
    color: #fff
}

.titre_slider{
    font-size:6vw;
    background: rgba(27, 55, 104, 0.5);
    border-radius: 30px;
}

.titre_slide{
    overflow:hidden;
    word-wrap: break-word;
}

.titre_mob{
    margin-top:-190px; overflow:hidden; word-wrap: break-word;
}

.titre_pro{
    line-height:normal;
    word-wrap: break-word;
    color: #666;
    font-size: 13px;
    margin:0;
}

.pres{
    background-position: center center;
    width: 100%;
    display: block;
     height: 514px;
     margin-top: 50px;
      color: #fff;
      margin-bottom:50px;
}

.titre_mob_home{
    font-size:6vw;
    background: rgba(27, 55, 104, 0.5);
    border-radius: 30px;
}

.taille_lire{
    font-size:13px;
}

.img{
    border-radius: 25px 10px;
}

.dsh-footer-bar-inner .center-block.col-md-8 {
    margin: 0 auto;
    width: 80%;
}
.info{
    font-size: 1.5vw;
    float: right;
    padding: 0 20px 0;
    font-weight: 400;
    margin: 0;
    height: 3.4vw;
    line-height: 3.4vw;
}

.texte{
    background-color: #282828;
    color: #fff;
    font-size: 1.5vw;
    font-weight: 400;
    margin: 0;
    height: 3.4vw;
    line-height: 3.4vw;
}

.info_mob{
    font-size: 5vw;
   text-align: center;
    font-weight: 400;

    height: 8vw;
    line-height: 8vw;
}

.texte_mob{
    background-color: #282828;
    color: #fff;
    font-size: 5vw;
    font-weight: 400;
    margin: 0;
    height: 8vw;
  
}
</style>

    <!-- Slider area -->
    <section class="slider_area row m0" >
        <div class="slider_inner camera_wrap" >
            @foreach($sliders as $slider)
            <div data-thumb="{{URL::to($slider->image)}}" data-src="{{URL::to($slider->image)}}">
                <!-- <div class="camera_caption">
                    <div class="row hidden-xs titre_slide"  >
                        <span class="titre_slider">&nbsp;<b>{{$slider->titre}}</b>&nbsp;</span>
                        <h4 class="wow fadeInUp animated color_slide" data-wow-delay="0.5s" >{{$slider->description}}</h4>
                    </div>

                     <div class="row hidden-lg hidden-md hidden-sm titre_mob">
                        <span class="titre_mob_home">&nbsp;<b>{{$slider->titre}}</b>&nbsp;</span>
                        <h4 class="wow fadeInUp animated color_slide" data-wow-delay="0.5s" >{{$slider->description}}</h4>
                    </div>
                </div> -->
            </div>
            @endforeach
        </div>

    </section>


    <!-- End Slider area -->

    <!-- bannière info -->

    <section>
        <div class="container">
      

            @foreach($infos as $info)
                <div class="hidden-xs">
                    <div class="col-md-2 col-xs-12 " style="background-color:{{$info->couleur}}; color:#fff">
                        <h3 class="info"><b>
                            @if (session('status') == 1)
                                    
                                Informations : 
                    
                            @else
                                News :
                            @endif
                         </b></h3>                             
                    </div>



                    <div class="col-md-8 col-xs-12 texte "  >
                    
                        <marquee onMouseOver="this.stop()" onMouseOut="this.start()">
                            @if (session('status') == 1)
                                    
                                {!!$info->texte!!}
                    
                            @else
                                {!!$info->texte_en!!}
                            @endif
                        </marquee>


                    </div>
                    <div class="col-md-2 col-xs-12 texte">

                            @if (session('status') == 1)

                                {{\Carbon\Carbon::parse($info->created_at)->format('d/m/Y')}}
                            @else
                                {{\Carbon\Carbon::parse($info->created_at)->format('Y/m/d')}}
                            @endif
                    
                       

                    
                    </div>
                </div>
                <div class="hidden-lg hidden-md hidden-sm">

                    <div class="col-md-2 col-xs-12 " style="background-color:{{$info->couleur}}; color:#fff">
                        <h3 class="info_mob"><b> 
                                @if (session('status') == 1)
                                    
                                    Informations : 
                        
                                @else
                                    News :
                                @endif
                        </b></h3>                             
                    </div>



                    <div class="col-md-8 col-xs-12 texte_mob "  >
                    
                        <marquee onMouseOver="this.stop()" onMouseOut="this.start()">
                        
                                @if (session('status') == 1)
                                    
                                    {!!$info->texte!!}
                        
                                @else
                                    {!!$info->texte_en!!}
                                @endif
                        </marquee>

                    
                    </div>
                    <div class="col-md-2 col-xs-12 texte_mob">
                    
                         @if (session('status') == 1)

                            {{\Carbon\Carbon::parse($info->created_at)->format('d/m/Y')}}
                        @else
                            {{\Carbon\Carbon::parse($info->created_at)->format('Y/m/d')}}
                        @endif
                    
                    </div>

                </div>
            @endforeach

    
        </div>
    </section>

    <!-- Fin bannière info -->

    <!-- Professional Builde -->
    <section class="building_construction_area">

     <div class="container">
            <div class="row building_construction_row">
            <div class="col-sm-2 constructing_right">

            </div>

              @foreach($professionals as $professional)

                <div class="col-sm-3 constructing_laft">

                    <div class="col-md-12 ipsum" >
                    <img src="{{URL::to($professional->image)}}" alt="{{$professional->titre}}" class="img">

                    </div>

                    <div class="col-md-12 ipsum "  align="center" >
                        <span class="titre_pro" align="justify">
                                @if (session('status') == 1)
                                    
                                    {{$professional->titre}}
                        
                                @else
                                    {{$professional->titre_en}}
                                @endif
                            
                        </span>
                    </div>
                    <div class="col-md-12 ipsum" align="center">
                    <a href="{{url('/details-professional/'.$professional->id)}}" class="taille_lire"> <strong>
                        @if (session('status') == 1)
                                    
                            Lire +
                
                        @else
                            Read +
                        @endif
                    </strong></a>
                    </div>
                </div>
                @endforeach

                <div class="col-sm-1 constructing_right">

                </div>
            </div>
        </div>




    </section>
    <!-- End Professional Builde -->

    <!-- About Us Area -->
    @foreach($presentations as $presentation)
     <section class="about_us_area row" class="pres">
        <div class="hidden-xs">
        <div class="container">

        <iframe width="100%" height="534" src="https://www.youtube.com/embed/{{$presentation->titre}}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>



        </div>
        </div>

         <div class="hidden-lg hidden-md hidden-sm">
        <div class="container">

        <iframe width="100%" height="auto" src="https://www.youtube.com/embed/{{$presentation->titre}}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>



        </div>
        </div>
    </section>

    @endforeach
    <!-- End About Us Area -->

@endsection

