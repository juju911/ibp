@extends('layouts.frontend')

@section('content')


<style>


#about {
    display: block;
    margin-top:5vw;
    margin-bottom: 10vw;
}

.subtittle h2:after {
    content: "";
    position: absolute;
    height: 2px;
    width: 80px;
    background: #222222;
    left: 0;
    bottom: -22px;
}

.subtittle h2 {
    font: 700 30px 'Avenir LT Std 55 Roman', sans-serif;
    color: #1b3768;
    text-transform: uppercase;
    position: relative;
    margin-bottom: 20px;
}

.subtittle{
    padding-bottom: 30px;
}

.justifie{
    text-align:justify;
}

.large{
    width:100%;
    height:auto;
}  

.marge{
    margin-bottom:15vw;
}

   html, body {
    height: 100%;
    font-family: 'Source Sans Pro', sans-serif;
    -webkit-font-smoothing: antialiased;
    font-synthesis: antialiased;
}

h1, h2, h3, h4, h5, h6 {
    margin: 0;
    font-weight: 600;
    color: #252525;
}

h3 {
    font-size: 30px;
}

p {
    font-size: 13.9px;
    color: #707070;
}

img {
    max-width: 100%;
}

input:focus, select:focus, button:focus, textarea:focus {
    outline: none;
}

a:hover, a:focus {
    text-decoration: none;
    outline: none;
}

ul, ol {
    padding: 0;
    margin: 0;
}

/*---------------------
    Helper CSS
-----------------------*/

.section-title {
    margin-bottom: 60px;
}

.section-title h3 {
    font-weight: 400;
    margin-bottom: 15px;
    text-transform: uppercase;
}

.section-title p {
    margin-bottom: 0;
} 

.text-white h3, .text-white p, .text-white span, .text-white li, .text-white a {
    color: #fff;
}


/*----------------
  About page
------------------*/

.team-member img {
    min-width: 100%;
}

.team-member:hover .member-pic:after {
    opacity: 0.5;
}

.team-member:hover .member-pic .member-social a {
    margin: 0 4px;
    bottom: 0;
    opacity: 1;
}

.member-pic {
    position: relative;
}

.member-pic:after {
    position: absolute;
    content: "";
    width: 100%;
    height: 100%;
    left: 0;
    top: 0;
    /* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#000000+0,30caa0+70 */
    background: black;
    /* Old browsers */
    /* FF3.6-15 */
    /* Chrome10-25,Safari5.1-6 */
    background: -webkit-gradient(linear, left top, left bottom, from(rgba(0, 0, 0, 0)), color-stop(80%, #0d42b3));
    background: -o-linear-gradient(top, rgba(0, 0, 0, 0) 0%, #0d42b3 80%);
    background: linear-gradient(to bottom, rgba(0, 0, 0, 0) 0%, #0d42b3 80%);
    /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#000000', endColorstr='#0d42b3', GradientType=0);
    /* IE6-9 */
    opacity: 0;
    -webkit-transition: all 0.5s;
    -o-transition: all 0.5s;
    transition: all 0.5s;
}

.member-pic .member-social {
    position: absolute;
    width: 100%;
    bottom: 20px;
    left: 0;
    text-align: center;
    z-index: 1;
}

.member-pic .member-social a {
    width: 30px;
    height: 30px;
    background: #fff;
    display: inline-block;
    text-align: center;
    border-radius: 50%;
    font-size: 12px;
    color: #707070;
    margin: 0 10px;
    padding-top: 6px;
    position: relative;
    bottom: -20px;
    opacity: 0;
    -webkit-transition: all 0.3s ease 0s;
    -o-transition: all 0.3s ease 0s;
    transition: all 0.3s ease 0s;
}

.member-pic .member-social a:hover {
    color: #0d42b3;
}

.member-info {
    padding: 28px 30px;
    -webkit-box-shadow: 0 5px 25px rgba(216, 216, 216, 0.2);
    box-shadow: 0 5px 25px rgba(216, 216, 216, 0.2);
}

.member-info h5 {
    font-weight: 400;
}

.member-info span {
    font-size: 14px;
    color: #707070;
}

.member-info .member-contact {
    margin-top: 16px;
    padding-top: 16px;
    position: relative;
}

.member-info .member-contact:after {
    content: "";
    position: absolute;
    width: 50px;
    height: 1px;
    top: 0;
    left: 0;
    background: #ebebeb;
}

.member-info .member-contact p {
    margin-bottom: 0;
    font-size: 16px;
    line-height: 1.8;
}

.member-info .member-contact p i {
    color: #0d42b3;
    margin-right: 10px;
}

/*------------------
    Responsive
---------------------*/

@media (min-width: 1200px) {
    .container {
        max-width: 1170px;
    }
}

</style>

    <!-- Banner area -->
  

    <!-- Building Construction Area -->
    <section aria-label="Detail" id="about">
                      <div class="container">

                           @foreach($abouts as $about)
                        <div class="row">

                        @if (session('status') == 1)
                
                            @if (($about->id % 2) == 0)
                            <!-- heading text -->   
                                <div class="col-md-6 marge">
                                    <div class="subtittle">
                                        <h2> {{$about->titre}}</h2>
                                    </div>
                                    <p class="justifie">
                                        {!!$about->description!!}
                                    </p>
                                    
                                </div>
                            <!-- heading text end --> 
                            
                                <div class="col-md-6">
                                    <img class="large" src="{{URL::to($about->image)}}" alt="img-responsive">
                                </div>

                            @else

                                <div class="col-md-6 marge">
                                    <img class="large" src="{{URL::to($about->image)}}" alt="img-responsive">                             
                                </div>
                            <!-- heading text end --> 
                            
                                <div class="col-md-6">
                                    <div class="subtittle">
                                        <h2> {{$about->titre}}</h2>
                                    </div>
                                    <p class="justifie">
                                        {!!$about->description!!}
                                    </p>
                                    
                                </div>
                            @endif

                        @else

                            @if (($about->id % 2) == 0)
                            <!-- heading text -->   
                                <div class="col-md-6 marge">
                                    <div class="subtittle">
                                        <h2> {{$about->titre_en}}</h2>
                                    </div>
                                    <p class="justifie">
                                        {!!$about->description_en!!}
                                    </p>
                                    
                                </div>
                            <!-- heading text end --> 
                            
                                <div class="col-md-6">
                                    <img class="large" src="{{URL::to($about->image)}}" alt="img-responsive">
                                </div>

                            @else

                                <div class="col-md-6 marge">
                                    <img class="large" src="{{URL::to($about->image)}}" alt="img-responsive">                             
                                </div>
                            <!-- heading text end --> 
                            
                                <div class="col-md-6">
                                    <div class="subtittle">
                                        <h2> {{$about->titre_en}}</h2>
                                    </div>
                                    <p class="justifie">
                                        {!!$about->description_en!!}
                                    </p>
                                    
                                </div>
                            @endif

                        @endif


                        
                          
                        </div>
                         @endforeach
                      </div>
                    </section>

                      <section class="team-section pb-0" style="background-color:#f0f0f0"  >
        <div class="container">
            <br><br>
            <div class="section-title text-center subtittle">
                <h2>
                    @if (session('status') == 1)
                
                        NOS DIRIGEANTS

                      @else

                        OUR OFFICERS
                    @endif
                </h2>
              
            </div>
            <div class="row text-center">
            <div class="col-lg-3 col-md-3 hidden-xs">

            </div>
            @foreach($partenaires as $p)
                
                <div class="col-lg-3 col-md-3">
                    <div class="team-member">
                        <div class="member-pic">
                            <img src="{{URL::to($p->image)}}" alt="{{$p->prenom}}">
                            <div class="member-social">
                                <a href="{{$p->fbk}}" target="_blank"><i class="fa fa-facebook"></i></a>
                                <a href="{{$p->lkd}}" target="_blank"><i class="fa fa-linkedin"></i></a>
                                <a href="{{$p->twitter}}" target="_blank"><i class="fa fa-twitter"></i></a>
                            </div>
                        </div>
                        <div class="member-info">
                            <h5>{{$p->prenom}}&nbsp;{{$p->link}}</h5>
                            <span>{{$p->fonction}}</span>
                            <div class="member-contact">
                                <p><i class="fa fa-plus-circle" style="width:20px"></i>
                                    <a href="{{url('/details-equipe/'.$p->id)}}">
                                        @if (session('status') == 1)
                
                                            EN SAVOIR +

                                        @else

                                           LEARN +

                                        @endif
                                        
                                    </a>
                                
                                </p>
                              
                            </div>
                        </div>
                    </div>
                </div>

            @endforeach
               
                  <div class="col-lg-3 col-md-3 hidden-xs">

                </div>
               
            </div>
        </div>
    </section>
   
   <section>
       <br><br>
       <br><br>
       <br><br>
       <br><br>
       <br><br>
       <br><br>
   </section>

  <script src="{{asset('index.js')}}"></script>

@endsection

<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/translate@1/translate.min.js"></script>