@extends('layouts.frontend')

@section('content')

<style>


#about {
    display: block;
    margin-top:5vw;
    margin-bottom: 10vw;
}

.subtittle h2:after {
    content: "";
    position: absolute;
    height: 2px;
    width: 80px;
    background: #222222;
    left: 0;
    bottom: -22px;
}

.subtittle h2 {
    font: 700 30px 'Avenir LT Std 55 Roman', sans-serif;
    color: #1b3768;
    text-transform: uppercase;
    position: relative;
    margin-bottom: 20px;
}

.subtittle{
    padding-bottom: 30px;
}

.droite{
    float:right;
}

.justifie{
    text-align:justify;
}

.large{
    width:100%;
    height:auto;
}  

.marge{
    margin-bottom:15vw;
}
</style>

    <!-- Banner area -->
    <section class="banner_area" data-stellar-background-ratio="0.5">
      <h2><b>
        @if (session('status') == 1)
                                    
            {{$professional->titre}}

        @else
            {{$professional->titre_en}}
        @endif
      </b></h2>
        
    </section>

    <!-- Building Construction Area -->
    <section aria-label="Detail" id="about">
                      <div class="container">
                        <div class="row">



                          <!-- heading text -->   
                          <div class="col-md-7 marge">
                          <div class="subtittle">
                                <h2>
                                @if (session('status') == 1)
                                    
                                    {{$professional->titre}}

                                @else
                                    {{$professional->titre_en}}
                                @endif </h2>
                            </div>
                            <p class="justifie">
                                @if (session('status') == 1)
                                    
                                    {!!$professional->description!!}
                        
                                @else
                                    {!!$professional->description_en!!}
                                @endif
                                
                            </p>
                            <br>
                            <small class="droite">
                                @if (session('status') == 1)
                                            
                                    Publié le: Publié le: {{\Carbon\Carbon::parse($professional->created_at)->format('d/m/Y')}}
            
                                @else
                                    Published on: {{\Carbon\Carbon::parse($professional->created_at)->format('Y/m/d')}}
                                @endif
                            </small>
                          </div>
                           <!-- heading text end --> 
                          
                          <div class="col-md-5">
                          <img class="large" src="{{URL::to($professional->image)}}" alt="img-responsive">
                          </div>
                          
                        </div>
                      </div>
                    </section>





    <!-- End Building Construction Area -->
@endsection
