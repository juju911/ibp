@extends('layouts.frontend')

@section('content')

<style>

.colorbtn{
    color: white !important;
}


.heightarea{
    height:120px 
}



</style>

    <!-- Banner area -->
    <section class="banner_area" data-stellar-background-ratio="0.5">
        <h2><b>
        @if (session('status') == 1)
                                
            Nous Contacter

        @else
            Contact us
        @endif 
        
       </b></h2>
           </section>
    <!-- End Banner area -->

    <!-- All contact Info -->
    <section class="all_contact_info">
        <div class="container">
            <div class="row contact_row">
                <div class="col-sm-6 contact_info">
                    <h2>
                        @if (session('status') == 1)
                                    
                            Info

                        @else
                           News
                        @endif 
                    </h2>
                    @foreach($contact as $c)
                    <div class="location">

                        @if (session('status') == 1)
                                    
                            {!! $c->content !!}

                        @else
                             {!! $c->content_en !!}
                        @endif 
                  
                       
                    </div>
                    @endforeach
                </div>
                <div class="col-sm-6 contact_info send_message">
                @include('includes.validator')
                    <h2>
                        @if (session('status') == 1)
                                    
                            Envoyez nous un message

                        @else
                            Send us a message
                        @endif
                    
                   </h2>
                    <form class="form-inline contact_box" action="{{url('/send-contact')}}" method="post">
                    @csrf
                        <input type="text" name="name" class="form-control input_box" placeholder="@if(session('status') == 1) Votre Nom * @else Your name * @endif" required>
                        <input type="email" name="mail" class="form-control input_box" placeholder="@if(session('status') == 1) Votre Email * @else Your email * @endif" required>
                        <input type="text" name="sujet" class="form-control input_box" placeholder="@if(session('status') == 1) Sujet @else Topic @endif"  required>
                        <textarea name="msg" class="form-control input_box heightarea" placeholder="@if(session('status') == 1) Message @else Message @endif" style="height:10vw;"></textarea>
                        <button type="submit" class="btn btn-default colorbtn">
                            @if (session('status') == 1)
                                    
                                Envoyer
    
                            @else
                                Send
                            @endif
                        </button>
                    </form>
                </div>
            </div>
        </div> 
    </section>
    <!-- End All contact Info -->

    <!-- Map -->
    <div class="contact_map" id="rollers">
        <iframe src="https://www.google.com/maps/d/embed?mid=13sCynpCJmi-0N0VqAVcA5JIUUCapx2do" width="640" height="480"></iframe>
    </div>
    <!-- End Map -->
@endsection
