@extends('layouts.frontend')

@section('content')

<style>
.actu_liste{
    list-style: none;
}

.actu_div{
    color: #666; 
    font-size: 13px; 
    line-height: 22px; 
    margin-top:20px; 
    margin-bottom:20px
}

.actu_titre{
    font-size:5vW; 
    line-height:normal
}

.img_actu{
    width:80%
}

.font_taille{
    font-weight: 600;
}

hr{
    width:50%;
    margin-bottom:100px;
}


</style>
    <!-- Banner area -->
    <section class="banner_area" >
        <h2>
            <b>
                @if (session('status') == 1)
					
                    Actualités

                @else

                    News
                @endif
            </b>
        </h2>
        <!-- <ol class="breadcrumb">
            <li><a href="{{url('/')}}">Acceuil</a></li>
            <li><a href="#" class="active">Actualité</a></li>
        </ol> -->
     
    </section>
    <!-- End Banner area -->

    <!-- About Us Area -->
    <section class="about_us_area row">
        <div class="container">

         
                @foreach($actualites as $actualite)
                <div >
                <li class="actu_liste">

                    @if (session('status') == 1)
					
                        <div class="actu_div" align="center">
                            <span>Publié le: {{\Carbon\Carbon::parse($actualite->created_at)->format('d/m/Y')}}</span><br>
                        <span class="actu_titre"> {{$actualite->title}}</span>
                        </div>

                        <div class="row ">
                            <div class="col-md-12 col-sm-6 " align="center">
                                <img class="img-responsive" src="{{URL::to($actualite->image)}}" style="width:80%" />
                                <p class="img_actu">
                                {!!str_limit($actualite->description,350)!!} &nbsp; 
                                <a href="{{url('/details-actualite/'.$actualite->id)}}" class="font_taille"> <strong>Lire +</strong></a>
                                </p>
                        
                            </div>
                        </div>

                    @else

                       <div class="actu_div" align="center">
                            <span>Published on: {{\Carbon\Carbon::parse($actualite->created_at)->format('Y/m/d')}}</span><br>
                        <span class="actu_titre"> {{$actualite->title_en}}</span>
                        </div>

                        <div class="row ">
                            <div class="col-md-12 col-sm-6 " align="center">
                                <img class="img-responsive" src="{{URL::to($actualite->image)}}" style="width:80%" />
                                <p class="img_actu">
                                {!!str_limit($actualite->description_en,350)!!} &nbsp; 
                                <a href="{{url('/details-actualite/'.$actualite->id)}}" class="font_taille"> <strong>Read +</strong></a>
                                </p>
                        
                            </div>
                        </div>
                    @endif

                        

                </li>
             <hr>
                @endforeach
                <!--li  class="timeline-inverted"-->
                <li class="clearfix" style="float: none; list-style: none;"></li>
         
                </div>
           
        </div>
    </section>
    <!-- End About Us Area -->

@endsection

