@extends('layouts.frontend')

@section('content')

<style>

.date_actu{
    float:right; 
    margin-top:30px
}

.other_actu{
    width: 120px; 
    height: 80px; 
    border-radius: 6px;
}


</style>
    <!-- Banner area -->
    <section class="banner_area" data-stellar-background-ratio="0.5">
        <h2><b>
                 @if (session('status') == 1)
					
                    Actualités

                @else

                    News
                @endif
        </b></h2>
        <!-- <ol class="breadcrumb">
            <li><a href="{{url('/')}}">Acceuil</a></li>
            <li><a href="#" class="active">Actualité</a></li>
        </ol> -->
    </section>

    <!-- Building Construction Area -->
    <section class="building_construction_area">
        <div class="container">
            <div class="row building_construction_row">
                <div class="col-sm-9 constructing_laft">
                    <h2>
                    @if (session('status') == 1)
					
                         {{$actualite->title}}

                      @else

                         {{$actualite->title_en}}
                    @endif
                   </h2>
                    <img src="{{URL::to($actualite->image)}}" alt="" >
                    @if (session('status') == 1)
					
                        <div class="col-md-12 ipsum" style="text-align: justify;">
                            {!!$actualite->description!!}
                            
                        </div>
                        <small class="date_actu">Publié le: {{\Carbon\Carbon::parse($actualite->created_at)->format('d/m/Y')}}</small>

                        @else

                            <div class="col-md-12 ipsum" style="text-align: justify;">
                                {!!$actualite->description_en!!}
                        
                            </div>
                            <small class="date_actu">Published on:: {{\Carbon\Carbon::parse($actualite->created_at)->format('Y/m/d')}}</small>
                    @endif
                    
                    <div class="col-md-6 ipsum_img"><img src="images/construction-2.jpg" alt=""></div>
                   
                   
                </div>
                <div class="col-sm-3 constructing_right">
                    <h3>
                        @if (session('status') == 1)
                        
                            Autres actualités

                        @else

                            Other news
                        @endif
                    </h3>
                    <br>
                    @if(count($others)>0)
                    <ul class="painting">
                        @foreach($others as $other)
                        <li>
                            <a href="{{url('/details-actualite/'.$other->id)}}">
                                <img src="{{URL::to($other->image)}}" alt="" class="other_actu"> 
                                @if (session('status') == 1)
                        
                                     {{$other->title}}

                                @else

                                   {{$other->title_en}}
                                @endif   
                                           
                            </a>
                        </li>
                        @endforeach
                    </ul>
                    @else
                        <p>
                        @if (session('status') == 1)
                        
                            Pas d'autres actualité

                        @else

                            No other news
                        @endif  
                             
                        </p>
                    @endif
                </div>
            </div>
        </div>
    </section>
    <!-- End Building Construction Area -->
@endsection
