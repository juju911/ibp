<!DOCTYPE html>
<html >
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="Site Pour Cabinet de conseil et accompagnement en stratégie">
    <meta property="og:title" content="IBP-Holding" />
    <meta property="og:type" content="Cabinet de conseil et accompagnement en stratégie" />
    <meta property="og:url" content="http://ibp-holding.com/" />
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>IBP-Holding</title>

    <!-- Favicon -->
    <link rel="icon" type="image/png" href="https://www.ibp-holding.com/images/favicon.ico"/>
    <!-- Bootstrap CSS -->
    <link href="{{asset('frontend/css/bootstrap.min.css')}}" rel="stylesheet">


    <!-- Animate CSS -->
    <link href="{{asset('frontend/vendors/animate/animate.css')}}" rel="stylesheet">
    <!-- Icon CSS-->
    <link rel="stylesheet" href="{{asset('frontend/vendors/font-awesome/css/font-awesome.min.css')}}">

    <!-- Camera Slider -->
    <link rel="stylesheet" href="{{asset('frontend/vendors/camera-slider/camera.css')}}">
    <!-- Owlcarousel CSS-->
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/vendors/owl_carousel/owl.carousel.css')}}" media="all">

    <!--Theme Styles CSS-->
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/css/style.css')}}" media="all" />

    <!--animate CSS-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.2/animate.min.css">

    <link rel='stylesheet' type='text/css' href='https://fonts.googleapis.com/css?family=Oswald:300,400,700|Roboto:300,300i,400,400i,500,500i,700,700i'>


    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="{{asset('frontend/js/html5shiv.min.js')}}"></script>
    <script src="{{asset('frontend/js/respond.min.js')}}"></script>
    <![endif]-->
    {!! Charts::styles() !!}
</head>
<body>

@include('includes.header')

<!-- Header_Area -->
<nav class="navbar navbar-default header_aera" id="main_navbar" style="z-index: 0;">

<!-- Menu for mobile -->

     <div class="container hidden-lg hidden-md " align="center">

<!-- Brand and toggle get grouped for better mobile display -->
<div class="col-md-1 col-sm-10 p0">
    <div class="navbar-header">

        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#min_navbarr">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>


         <a  href="{{url('/')}}"><img src="{{URL::to('frontend/images/Logo1.png')}}" alt="" style="width: 150px; float:left"></a>


    </div>
</div>

<!-- Collect the nav links, forms, and other content for toggling -->
<div class="col-md-10 hidden-sm" style="width:100%">
    <div class="collapse navbar-collapse" id="min_navbarr">
        <ul class="nav navbar-nav navbar-center ">
            <li class="{{$menu == 'acceuil'?'header_active':''}}">
                <a href="{{url('/')}}">
                    @if (session('status') == 1)
                                                            
                         Accueil

                    @else
                        
                        Home
                    @endif
                </a>
            </li>
            <li class="{{$menu == 'actualite'?'header_active':''}}" >
                <a href="{{route('actualite')}}" >
                     @if (session('status') == 1)
                                                            
                        Actualités

                    @else
                        
                        News
                    @endif
                </a>
            </li>
            <li class="{{$menu == 'a-propos'?'header_active':''}}">
                <a href="{{route('about')}}" >
                @if (session('status') == 1)
                                                            
                        L'Entreprise

                    @else
                        
                        Company
                    @endif
                </a>
            </li>
            <li class="{{$menu == 'offre'?'header_active':''}}">
                <a href="{{route('offre')}}" >
                     @if (session('status') == 1)
                                                            
                        Carrière

                    @else
                        
                        Carrer
                    @endif
                </a>
            </li>

            <li class="{{$menu == 'service'?'header_active':''}}">
                <a href="{{route('service')}}" >
                     @if (session('status') == 1)
                                                            
                        Services

                    @else
                        
                        Services
                    @endif
                </a>
            </li>
            <li class="{{$menu == 'blog'?'header_active':''}}">
                <a href="{{route('blog')}}" >
                @if (session('status') == 1)
                                                            
                    Blog

                @else
                    
                    Blog
                @endif
                </a>
            </li>
            <li class="{{$menu == 'sondage'?'header_active':''}}">
                <a href="{{route('sondage')}}">
                @if (session('status') == 1)
                                                            
                    Sondages

                @else
                    
                    Surveys
                @endif
                </a>
            </li>
            <li class="{{$menu == 'contact'?'header_active':''}}">
                <a href="{{route('contact')}}">
                @if (session('status') == 1)
                                                            
                    Contacts

                @else
                    
                    Contacts
                @endif
                </a>
            </li>

        </ul>
    </div><!-- /.navbar-collapse -->
</div>

<div class="col-md-1 col-sm-10 p0"></div>
</div>


<!-- Menu for mobile -->
    <div class="container hidden-xs" >

        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="col-md-1 col-sm-10 p0">
            <div class="navbar-header">

                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#min_navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

            </div>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="col-md-10 col-sm-10 p0" style="width:100%" >
            <div class="collapse navbar-collapse" id="min_navbar">
                <ul class="nav navbar-nav navbar-center " align="center">
                    <li style="margin-left: 25px;" class="{{$menu == 'acceuil'?'header_active':''}}">
                        <a href="{{url('/')}}">
                        @if (session('status') == 1)
                                                            
                            Accueil

                        @else
                            
                            Home
                        @endif
                        </a>
                    </li>
                    <li style="margin-left: 25px;" class="{{$menu == 'actualite'?'header_active':''}}" >
                        <a href="{{route('actualite')}}" >
                             @if (session('status') == 1)
                                                            
                                Actualités

                            @else
                                
                                News
                            @endif
                        </a>
                    </li>
                    <li style="margin-left: 25px;" class="{{$menu == 'a-propos'?'header_active':''}}">
                        <a href="{{route('about')}}" >
                        @if (session('status') == 1)
                                                            
                            L'Entreprise

                        @else
                            
                            Company
                        @endif
                        </a>
                    </li>
                    <li style="margin-left: 25px;" class="{{$menu == 'offre'?'header_active':''}}">
                        <a href="{{route('offre')}}" >
                             @if (session('status') == 1)
                                                            
                                Carrière

                            @else
                                
                                Carrer
                            @endif
                        </a>
                    </li>
                    <li style="margin-left: 25px;"><a  href="{{url('/')}}"><img src="{{URL::to('frontend/images/Logo1.png')}}" alt="" style="width: 150px"></a></li>
                    <li style="margin-left: 25px;" class="{{$menu == 'service'?'header_active':''}}">
                        <a href="{{route('service')}}" >
                             @if (session('status') == 1)
                                                            
                                Services

                            @else
                                
                                Services
                            @endif
                        </a>
                    </li>
                    <li style="margin-left: 25px;" class="{{$menu == 'blog'?'header_active':''}}">
                        <a href="{{route('blog')}}" >
                        @if (session('status') == 1)
                                                            
                            Blog
    
                        @else
                            
                            Blog
                        @endif
                        </a>
                    </li>
                    <li style="margin-left: 25px;" class="{{$menu == 'sondage'?'header_active':''}}">
                        <a href="{{route('sondage')}}">
                        @if (session('status') == 1)
                                                            
                            Sondages

                        @else
                            
                            Surveys
                        @endif
                        </a>
                    </li>
                    <li style="margin-left: 25px;" class="{{$menu == 'contact'?'header_active':''}}">
                        <a href="{{route('contact')}}">
                        @if (session('status') == 1)
                                                            
                            Contacts
    
                        @else
                            
                            Contacts
                        @endif
                        </a>
                    </li>

                </ul>
            </div><!-- /.navbar-collapse -->
        </div>

        <div class="col-md-1 col-sm-10 p0"></div>
    </div><!-- /.container -->
</nav>
<!-- End Header_Area -->
@yield('content')

@include('includes.footer')

<!-- jQuery JS -->
<script src="{{asset('frontend/js/jquery-1.12.0.min.js')}}"></script>
<!-- Bootstrap JS -->
<script src="{{asset('frontend/js/bootstrap.min.js')}}"></script>
<!-- Animate JS -->
<script src="{{asset('frontend/vendors/animate/wow.min.js')}}"></script>
<!-- Camera Slider -->
<script src="{{asset('frontend/vendors/camera-slider/jquery.easing.1.3.js')}}"></script>
<script src="{{asset('frontend/vendors/camera-slider/camera.min.js')}}"></script>
<!-- Isotope JS -->
<script src="{{asset('frontend/vendors/isotope/imagesloaded.pkgd.min.js')}}"></script>
<script src="{{asset('frontend/vendors/isotope/isotope.pkgd.min.js')}}"></script>
<!-- Progress JS -->
<script src="{{asset('frontend/vendors/Counter-Up/jquery.counterup.min.js')}}"></script>
<script src="{{asset('frontend/vendors/Counter-Up/waypoints.min.js')}}"></script>
<!-- Owlcarousel JS -->
<script src="{{asset('frontend/vendors/owl_carousel/owl.carousel.min.js')}}"></script>
<!-- Stellar JS -->
<script src="{{asset('frontend/vendors/stellar/jquery.stellar.js')}}"></script>
<!-- Theme JS -->
<script src="{{asset('frontend/js/theme.js')}}"></script>

<script>
    //var bgcolorlist = ["#1B3768", "#1B3768", "#3C50D1"];
    //$(".header_aera").css("background-color",bgcolorlist[Math.floor(Math.random()*bgcolorlist.length)]);

    // accordion pluse minus goes here
    jQuery('.accordion-toggle').click(function(){
        var has = jQuery(this);
        if(has.hasClass('collapsed')){
            jQuery(this).find('i').removeClass('fa-plus');
            jQuery(this).find('i').addClass('fa-minus');
        }
        else{
            jQuery(this).find('i').removeClass('fa-minus');
            jQuery(this).find('i').addClass('fa-plus');
        }
    })
</script>
</body>
</html>
