<!-- Preloader -->
<div class="preloader"></div>

<!-- Top Header_Area -->
<section class="top_header_area">
    <div class="container">
        <ul class="nav navbar-nav top_nav">
            <li><a href="#"><i class="fa fa-phone"></i>+225 22 59 19 08 / +225 22 59 28 97</a></li>
            <li><a href="mailto:info@ibp-holding.com"><i class="fa fa-envelope-o"></i>info@ibp-holding.com</a></li>
            <li><a href="#"><i class="fa fa-clock-o"></i>
            @if (session('status') == 1)
                                                            
                 Lundi - Vendredi 8:00 - 17:00

            @else
                
                Monday - Friday 8AM - 5PM
            @endif
            </a></li>
        </ul>
        <ul class="nav navbar-nav navbar-right social_nav">
            @foreach($socials as $s)
               <li><a href="{{$s->link}}" target="_blank"><i class="{{$s->logo}}" aria-hidden="true"></i></a></li>
            @endforeach
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            @foreach($flags as $f)
        <li>
             @if(($f->status)==1)
                <a href="{{route('flag',$f->id)}}"><img src="/images/united.png" alt="" style="height: 20px;margin-left: 10px"></a>
            @else
                <a href="{{route('flag',$f->id)}}"><img src="/images/france.png" alt="" style="height: 20px;margin-left: 10px"></a>
            @endif
     
            
            </li>
           
        @endforeach
           
        </ul>
  

      
        
    </div>
</section>
<!-- End Top Header_Area -->
