<!-- Footer Area -->
<footer class="footer_area">
    <div class="container">
        <div class="footer_row row">
            <div class="col-md-3 col-sm-6 col-xs-12 footer_about">
                @foreach($ibpfooters as $i)
           
                <h2>
                @if (session('status') == 1)
                                                            
                    {{$i->titre}}

                @else
                    
                    {{$i->titre_en}}
                @endif
                </h2>
                <img src="{{URL::to('frontend/images/Logo1.png')}}" alt="img-responsive">
                <p>
                @if (session('status') == 1)
                                                            
                    {!!$i->description!!}

                @else
                    
                    {!!$i->description_en!!}
                @endif
                
                </p>
           
            @endforeach
                <ul class="socail_icon">
                    @foreach($socials as $s)
                        <li><a href="{{$s->link}}" target="_blank"><i class="{{$s->logo}}" aria-hidden="true"></i></a></li>
                    @endforeach
                </ul>
            </div>
            <div class="col-md-2 col-sm-3 col-xs-6 footer_about quick">
                <h2>
                @if (session('status') == 1)
                                                            
                    Liens utiles

                @else
                    
                    Useful links
                @endif
                
                </h2>
                <ul class="quick_link">
                    <li><a href="{{route('home')}}"><i class="fa fa-chevron-right"></i>
                    @if (session('status') == 1)
                                                            
                         Accueil

                    @else
                        
                        Home
                    @endif
                    
                    </a></li>
                    <li><a href="{{route('actualite')}}"><i class="fa fa-chevron-right"></i>
                    @if (session('status') == 1)
                                                            
                        Actualités

                    @else
                        
                        News
                    @endif
                    </a></li>
                    <li><a href="{{route('about')}}"><i class="fa fa-chevron-right"></i>
                    @if (session('status') == 1)
                                                            
                        L'Entreprise

                    @else
                        
                        Company
                    @endif
                    </a></li>
                    <li><a href="{{route('offre')}}"><i class="fa fa-chevron-right"></i>
                    @if (session('status') == 1)
                                                            
                        Carrière

                    @else
                        
                        Carrer
                    @endif
                    </a></li>
                  
                   
                </ul>
            </div>

             <div class="col-md-2 col-sm-3 col-xs-6 footer_about quick">
                <h2>.</h2>
                <ul class="quick_link">
                    <li><a href="{{route('service')}}"><i class="fa fa-chevron-right"></i>
                    @if (session('status') == 1)
                                                            
                        Services

                    @else
                        
                        Services
                    @endif
                    </a></li>
                    <li><a href="{{route('blog')}}"><i class="fa fa-chevron-right"></i>
                    @if (session('status') == 1)
                                                            
                        Blog

                    @else
                        
                        Blog
                    @endif
                    </a></li>
                    <li><a href="{{route('sondage')}}"><i class="fa fa-chevron-right"></i>
                    @if (session('status') == 1)
                                                            
                        Sondages

                    @else
                        
                        Surveys
                    @endif
                    </a></li>
                    <li><a href="{{route('contact')}}"><i class="fa fa-chevron-right"></i>
                    @if (session('status') == 1)
                                                            
                        Contacts

                    @else
                        
                        Contacts
                    @endif
                    </a></li>
                    <!-- <li><a href="{{route('faq')}}"><i class="fa fa-chevron-right"></i>Faq</a></li> -->
                </ul>
            </div>

             <div class="col-md-5 col-sm-6 col-xs-12 footer_about">
            @foreach($contactfooters as $c)
                <h2>
                @if (session('status') == 1)
                                                            
                    {{$c->titre}}

                @else
                    
                    {{$c->titre_en}}
                @endif
                
                </h2>
                <address>
                    <p>
                    @if (session('status') == 1)
                                                            
                        {{$c->description}}
    
                    @else
                        
                         {{$c->description_en}}
                    @endif
                    </p>
                    <ul class="my_address">
                        <li><a href="mailto:{{$c->email}}"><i class="fa fa-envelope" aria-hidden="true"></i>{{$c->email}}</a></li>
                        <li><a href="#"><i class="fa fa-phone" aria-hidden="true"></i>{{$c->telephone}}</a></li>
                        <li><a href="{{route('contact')}}#rollers"><i class="fa fa-map-marker" aria-hidden="true"></i><span>{!!$c->adresse!!} </span></a>
                        </li>
                    </ul>
                </address>
            @endforeach
            </div>
        </div>
    </div>
    <div class="copyright_area">
    @if (session('status') == 1)
                                                            
        
        Copyright 2019 Tous droits réservés. Conçu par <a href="">ChapChap.</a>

    @else
        
        Copyright 2019 All rights reserved. Designed by <a href="">ChapChap.</a>
    @endif
       
    </div>
</footer>
<!-- End Footer Area -->
