<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ContactMail extends Mailable
{
    use Queueable, SerializesModels;



    
    /**
     * The order instance.
     *
     * @var Nom
     */
    public $nom;

    /**
     * The order instance.
     *
     * @var Mail
     */
    public $mail;

     /**
     * The order instance.
     *
     * @var Sujet
     */
    public $sujet;

     /**
     * The order instance.
     *
     * @var Msg
     */
    public $msg;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($nom, $mail, $sujet, $msg)
    {
        $this->nom = $nom;
        $this->mail = $mail;
        $this->sujet = $sujet;
        $this->msg = $msg;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('backend.mail');
        
    }
}
