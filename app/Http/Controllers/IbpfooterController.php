<?php

namespace App\Http\Controllers;

use App\Ibpfooter;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DOMDocument;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

class IbpfooterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->AdminAuthCheck();
        $ibpfooters=DB::table('ibpfooters')
            ->get();
       return view('backend.ibpfooter-liste',compact('ibpfooters'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->AdminAuthCheck();
        return view('backend.ibpfooter-add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $titre=$request->titre;
        $description=$request->description;
        $titre_en=$request->titreen;
        $description_en=$request->descriptionen;

        $ibpfooter = new Ibpfooter();
        $ibpfooter->titre = $titre;
        $ibpfooter->description = $description;
        $ibpfooter->titre_en = $titre_en;
        $ibpfooter->description_en = $description_en;

        $ibpfooter->save();
        return redirect()->route('ibpfooter.index')->with(['message' => ' crée avec succes']);
   
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Ibpfooter  $ibpfooter
     * @return \Illuminate\Http\Response
     */
    public function show(Ibpfooter $ibpfooter)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Ibpfooter  $ibpfooter
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['ibpfooter'] = Ibpfooter::find($id);
        return view('backend.ibpfooter-edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Ibpfooter  $ibpfooter
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Ibpfooter $ibpfooter)
    {
    
        $titre=$request->titre;
        $description=$request->description;
        $titre_en=$request->titreen;
        $description_en=$request->descriptionen;

        $ibpfooter = Ibpfooter::find($request->id);
        $ibpfooter->titre = $titre;
        $ibpfooter->description = $description;
        $ibpfooter->titre_en = $titre_en;
        $ibpfooter->description_en = $description_en;
        

        $ibpfooter->update();
        return redirect()->route('ibpfooter.index')->with(['message' => 'modifiée avec succes']);
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Ibpfooter  $ibpfooter
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Ibpfooter::destroy($id)){
            return redirect()->route('ibpfooter.index')->with(['message' => 'supprimé avec succes']);
        }else{
            return back()->with(['danger' => 'Erreur dans la suppression']);
        }
    }

    public function AdminAuthCheck(){
        $admin_id=Session::get('admin_id');
        if ($admin_id) {
            return;
        }else {
            return Redirect::to('/login')->send();
        }
    }
}
