<?php

namespace App\Http\Controllers;

use App\cv;
use App\Lm;
use App\offre;
use App\Toffre;
use App\Social;
use App\Flag;
use App\Ibpfooter;
use App\Contactfooter;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class OffreController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $offres=DB::table('offres')
            ->get();
        
        return view('backend.offre',compact('offres'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.add-offre');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
          $this->validate($request, [
            'poste' => 'required',
            'posteen' => 'required',
            'description' => 'required',
            'descriptionen' => 'required',
            'type' => 'required',
            'detail' => 'required',
            'detailen' => 'required'
        ]);

        $poste = $request['poste'];
        $poste_en = $request['posteen'];
        $type = $request['type'];
        $description = $request['description'];
        $description_en = $request['descriptionen'];
        $detail = $request['detail'];
        $detail_en = $request['detailen'];

        $offre = new offre();
        $offre->poste=$poste;
        $offre->poste_en=$poste_en;
        $offre->type=$type;
        $offre->description=$description;
        $offre->description_en=$description_en;
        $offre->detail=$detail;
        $offre->detail_en=$detail_en;
        $offre->save();

        return redirect()->route('dash.offre')->with(['message' => 'Nouvelle offre ajouté avec succes']);
    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function show($id)
    {

         $data['offre']=DB::table('offres')
            ->where('id',$id)
            ->first();
        $data['socials']=Social::all();
        $data['flags']=Flag::all();
        $data['others']=DB::table('offres')
            ->where('id','!=',$id)
            ->get();
        $data['ibpfooters']=Ibpfooter::all();
        $data['contactfooters']=Contactfooter::all();
        $data['menu']="carriere";

        return view('frontend.details-offre',$data);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\offre  $offre
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function edit($id)
    {
        $data['offre']=offre::find($id);
        return view('backend.edit-offre',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\offre $offre
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, offre $offre)
    {
         $this->validate($request, [
            'poste' => 'required',
            'posteen' => 'required',
            'description' => 'required',
            'descriptionen' => 'required',
            'type' => 'required',
            'detail' => 'required',
            'detailen' => 'required'
        ]);

        $poste = $request['poste'];
        $poste_en = $request['posteen'];
        $type = $request['type'];
        $description = $request['description'];
        $description_en = $request['descriptionen'];
        $detail = $request['detail'];
        $detail_en = $request['detailen'];

        $offre = offre::find($request['id']);
        $offre->poste=$poste;
        $offre->poste_en=$poste_en;
        $offre->type=$type;
        $offre->description=$description;
        $offre->description_en=$description_en;
        $offre->detail=$detail;
        $offre->detail_en=$detail_en;
        $offre->update();

        return redirect()->route('dash.offre')->with(['message' => 'Offre modifié avec succes']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\offre  $offre
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('offres')
            ->where('id', $id)
            ->delete();
        cv::where('offre',$id)->delete();

        return redirect()->route('dash.offre')->with(['message' => 'Offre supprimé avec succes']);
    }

    public function cv(){
        $cvs=DB::table('cvs')->get();
        return view('backend.postulants',compact('cvs'));
    }

    public function lm(){
        $lms=DB::table('lms')->get();
        return view('backend.postulantst',compact('lms'));
    }

    public function store_cv(Request $request){

        $this->validate($request, [
            'name' => 'required',
            'cv' => 'required',
        ]);
            
        $name = $request['name'];
        $offre = $request['offre'];


        $cv= new cv();
        $cv->nom=$name;
        $cv->offre=$offre;


        $cv_file = $request->file('cv');

        if ($cv_file)
        {
            $filename = $cv_file->getClientOriginalName();
            $ext = strtolower($cv_file->getClientOriginalExtension());
            $image_full_name = $name.'.'.$ext;
            $upload_path = 'cv/';
            $post_cv = $upload_path . $image_full_name;
            $success = $cv_file->move($upload_path, $image_full_name);

            if ($success) {
                $cv->cv=$post_cv;
                $cv->save();
            }
        }
        return redirect()->route('offre')->with(['message' => 'Votre Cv a été envoyé avec succes']);
    }

    public function store_lm(Request $request){

        $this->validate($request, [
            'name' => 'required',
            'tel' => 'required',
            'mail' => 'required',
            'type' => 'required',
            'cv' => 'required',
            'lm' => 'required',
        ]);

        $name = $request['name'];
        $tel = $request['tel'];
        $mail = $request['mail'];
        $type = $request['type'];


        $lm= new Lm();
        $lm->nom=$name;
        $lm->tel=$tel;
        $lm->mail=$mail;
        $lm->type=$type;
       


        $cv_file = $request->file('cv');
        $lm_file = $request->file('lm');
       
        if ($cv_file)
        {
            $filename = $cv_file->getClientOriginalName();
            $ext = strtolower($cv_file->getClientOriginalExtension());
            $image_full_name = $name.$filename.'cv'.'.'.$ext;
            $upload_path = 'lm/';
            $post_cv = $upload_path . $image_full_name;
            $success = $cv_file->move($upload_path, $image_full_name);

            if ($success) {
                
                $lm->cv=$post_cv;
              
                
            }
        }

        if ($lm_file)
        {
            $filename = $lm_file->getClientOriginalName();
            $ext = strtolower($lm_file->getClientOriginalExtension());
            $image_full_name = $name.$filename.'lm'.'.'.$ext;
            $upload_path = 'lm/';
            $post_lm = $upload_path . $image_full_name;
            $success = $lm_file->move($upload_path, $image_full_name);

            if ($success) {
                $lm->lm=$post_lm;
                
            }
        }
        
        $lm->save();
        return redirect()->route('offre')->with(['message' => 'Votre demande a été envoyé avec success']);
    }

    public function deletePostulant($id){
        if (cv::destroy($id)){
            return redirect()->route('cv')->with(['message' => 'Postulant supprimé avec succes']);
        }else{
            return back()->with(['danger' => 'Erreur dans la suppression']);
        }
    }
}
