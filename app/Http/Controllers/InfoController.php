<?php

namespace App\Http\Controllers;

use App\Info;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DOMDocument;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

class InfoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->AdminAuthCheck();
        $infos=DB::table('infos')
            ->get();
       return view('backend.info-liste',compact('infos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->AdminAuthCheck();
        return view('backend.info-add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $texte=$request->texte;
        $texte_en=$request->texteen;
        $couleur=$request->couleur;

        $info = new Info();
        $info->texte = $texte;
        $info->texte_en = $texte_en;
        $info->couleur = $couleur;

        $info->save();
        return redirect()->route('info.index')->with(['message' => ' crée avec succes']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\info  $info
     * @return \Illuminate\Http\Response
     */
    public function show(info $info)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\info  $info
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['info'] = Info::find($id);
        return view('backend.info-edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\info  $info
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, info $info)
    {
        $texte=$request->texte;
        $texte_en=$request->texteen;
        $couleur=$request->couleur;

        $info = Info::find($request->id);
        $info->texte = $texte;
        $info->texte_en = $texte_en;
        $info->couleur = $couleur;

        $info->update();
        return redirect()->route('info.index')->with(['message' => 'modifiée avec succes']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\info  $info
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Info::destroy($id)){
            return redirect()->route('info.index')->with(['message' => 'supprimé avec succes']);
        }else{
            return back()->with(['danger' => 'Erreur dans la suppression']);
        }
    }

    public function AdminAuthCheck(){
        $admin_id=Session::get('admin_id');
        if ($admin_id) {
            return;
        }else {
            return Redirect::to('/login')->send();
        }
    }
}
