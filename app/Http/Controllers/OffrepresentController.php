<?php

namespace App\Http\Controllers;

use App\offrepresent;
use Illuminate\Http\Request;

class OffrepresentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\offrepresent  $offrepresent
     * @return \Illuminate\Http\Response
     */
    public function show(offrepresent $offrepresent)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\offrepresent  $offrepresent
     * @return \Illuminate\Http\Response
     */
    public function edit(offrepresent $offrepresent)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\offrepresent  $offrepresent
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, offrepresent $offrepresent)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\offrepresent  $offrepresent
     * @return \Illuminate\Http\Response
     */
    public function destroy(offrepresent $offrepresent)
    {
        //
    }
}
