<?php

namespace App\Http\Controllers;

use App\Contactfooter;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DOMDocument;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

class ContactfooterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->AdminAuthCheck();
        $contactfooters=DB::table('contactfooters')
            ->get();
       return view('backend.contactfooter-liste',compact('contactfooters'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->AdminAuthCheck();
        return view('backend.contactfooter-add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $titre=$request->titre;
        $description=$request->description;
        $email=$request->email;
        $telephone=$request->telephone;
        $adresse=$request->adresse;
        $titre_en=$request->titreen;
        $description_en=$request->descriptionen;

        $contactfooter = new Contactfooter();
        $contactfooter->titre = $titre;
        $contactfooter->description = $description;
        $contactfooter->titre_en = $titre_en;
        $contactfooter->description_en = $description_en;
        $contactfooter->email = $email;
        $contactfooter->telephone = $telephone;
        $contactfooter->adresse = $adresse;

        $contactfooter->save();
        return redirect()->route('contactfooter.index')->with(['message' => ' crée avec succes']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Contactfooter  $contactfooter
     * @return \Illuminate\Http\Response
     */
    public function show(Contactfooter $contactfooter)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Contactfooter  $contactfooter
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['contactfooter'] = Contactfooter::find($id);
        return view('backend.contactfooter-edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Contactfooter  $contactfooter
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Contactfooter $contactfooter)
    {
        $titre=$request->titre;
        $description=$request->description;
        $titre_en=$request->titreen;
        $description_en=$request->descriptionen;
        $email=$request->email;
        $telephone=$request->telephone;
        $adresse=$request->adresse;

        $contactfooter = Contactfooter::find($request->id);
        $contactfooter->titre = $titre;
        $contactfooter->description = $description;
        $contactfooter->titre_en = $titre_en;
        $contactfooter->description_en = $description_en;
        $contactfooter->email = $email;
        $contactfooter->telephone = $telephone;
        $contactfooter->adresse = $adresse;
        

        $contactfooter->update();
        return redirect()->route('contactfooter.index')->with(['message' => 'modifiée avec succes']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Contactfooter  $contactfooter
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Contactfooter::destroy($id)){
            return redirect()->route('contactfooter.index')->with(['message' => 'supprimé avec succes']);
        }else{
            return back()->with(['danger' => 'Erreur dans la suppression']);
        }
    }

    public function AdminAuthCheck(){
        $admin_id=Session::get('admin_id');
        if ($admin_id) {
            return;
        }else {
            return Redirect::to('/login')->send();
        }
    }
}
