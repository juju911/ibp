<?php

namespace App\Http\Controllers;

use App\Contact;
use App\Ibpfooter;
use App\Contactfooter;
use App\Social;
use App\Flag;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DOMDocument;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Mail;
use App\Mail\ContactMail;

class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      
        $this->AdminAuthCheck();
        $contacts=DB::table('contacts')
            ->get();
       return view('backend.contact-liste',compact('contacts'));
    }

    public function contact(){
        $data['menu']='contact';
        $data['socials']=Social::all();
        $data['flags']=Flag::all();
        $data['ibpfooters']=Ibpfooter::all();
        $data['contactfooters']=Contactfooter::all();
        $data['contact']=Contact::all();
        return view('frontend.contact',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->AdminAuthCheck();
        return view('backend.contact-add');
    }

  

    public function sendmail(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'mail' => 'required',
            'sujet' => 'required',
            'msg' => 'required'
        ]);

        $mailable= new ContactMail($request->name,$request->mail,$request->sujet,$request->msg);
      

        
        Mail::to("info@ibp-holding.com")->send($mailable);
        return redirect()->route('contact')->with(['message' => 'Mail envoyé avec succes']);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $content=$request->content;
        $content_en=$request->contenten;

        $contact = new Contact();
        $contact->content = $content;
        $contact->content_en = $content_en;

        $contact->save();
        return redirect()->route('contact.index')->with(['message' => ' crée avec succes']);
     }

    /**
     * Display the specified resource.
     *
     * @param  \App\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function show(Contact $contact)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['contact'] = Contact::find($id);
        return view('backend.contact-edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Contact $contact)
    {
        $content=$request->content;
        $content_en=$request->contenten;
       

        $contact = Contact::find($request->id);
        $contact->content = $content;
        $contact->content_en = $content_en;
      
        

        $contact->update();
        return redirect()->route('contact.index')->with(['message' => 'modifiée avec succes']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Contact::destroy($id)){
            return redirect()->route('contact.index')->with(['message' => 'supprimé avec succes']);
        }else{
            return back()->with(['danger' => 'Erreur dans la suppression']);
        }
    }

    public function AdminAuthCheck(){
        $admin_id=Session::get('admin_id');
        if ($admin_id) {
            return;
        }else {
            return Redirect::to('/login')->send();
        }
    }
}
