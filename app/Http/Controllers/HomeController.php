<?php

namespace App\Http\Controllers;

use App\Blog;
use App\Partenaire;
use App\Presentation;
use App\Projet;
use App\Toffre;
use App\Service;
use App\Info;
use App\Social;
use App\Flag;
use App\Ibpfooter;
use App\Contactfooter;
use App\About;
use App\Professional;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

class HomeController extends Controller
{
    public function home(){

        $data['menu']='acceuil';
        $data['articles']=DB::table('blogs')
            ->where('status','=',1)
            ->take(4)
            ->get();
        $data['sliders']=DB::table('sliders')
            ->orderByDesc('id')
            ->get();
        $data['services']=Service::take(6)->orderBy('id','desc')->get();
        $data['socials']=Social::all();
        $data['flags']=Flag::all();
        $data['infos']=Info::all();
        $data['ibpfooters']=Ibpfooter::all();
        $data['contactfooters']=Contactfooter::all();
        $data['professionals']=Professional::take(3)->orderByDesc('id')->get();
        $data['presentations']=Presentation::take(1)->orderByDesc('id')->get();
        $data['articles'] = Blog::whereStatus(0)->take(4)->orderBy('id','desc')->get();
        $data['partenaires']=Partenaire::all();
        $data['projects']=DB::table('projets')
            ->orderByDesc('id')->take(12)->get();
        return view('frontend.home',$data);
    }

    public function contact(){
        $data['menu']='contact';
        $data['socials']=Social::all();
        $data['flags']=Flag::all();
        $data['ibpfooters']=Ibpfooter::all();
        $data['contactfooters']=Contactfooter::all();
        $data['data']=DB::table('contacts')->first();
        return view('frontend.contact',$data);
    }

    public function about(){
        $data['menu']='a-propos';
        $data['socials']=Social::all();
        $data['flags']=Flag::all();
        $data['ibpfooters']=Ibpfooter::all();
        $data['contactfooters']=Contactfooter::all();
        $data['partenaires']=Partenaire::take(2)->orderByDesc('id')->get();
        $data['abouts']=About::take(3)->orderByDesc('id')->get();
        return view('frontend.about',$data);
    }

    public function actualite(){
        $data['menu']='actualite';
        $data['socials']=Social::all();
        $data['flags']=Flag::all();
        $data['ibpfooters']=Ibpfooter::all();
        $data['contactfooters']=Contactfooter::all();
        $data['actualites']=DB::table('actualites')->orderByDesc('id')->get();
        return view('frontend.actualite',$data);
    }

    public function carriere(){
        $data['menu']='offre';
        $data['socials']=Social::all();
        $data['flags']=Flag::all();
        $data['ibpfooters']=Ibpfooter::all();
        $data['contactfooters']=Contactfooter::all();
        $data['partenaires']=Partenaire::all();
        return view('frontend.carriere',$data);
    }

    public function projet(){
        $data['projects']=Projet::all();
        $data['socials']=Social::all();
        $data['flags']=Flag::all();
        $data['ibpfooters']=Ibpfooter::all();
        $data['contactfooters']=Contactfooter::all();
        $data['menu']='projet';
        return view('frontend.projet',$data);
    }

    public function offre(){
        $data['menu']='offre';
        $data['socials']=Social::all();
        $data['flags']=Flag::all();
        $data['ibpfooters']=Ibpfooter::all();
        $data['contactfooters']=Contactfooter::all();
        $data['offres']=DB::table('offres')->where('statut','=',0)->get();
        return view('frontend.offres',$data);
    }

    public function toffre(){
        $data['menu']='offre';
        $data['socials']=Social::all();
        $data['flags']=Flag::all();
        $data['ibpfooters']=Ibpfooter::all();
        $data['contactfooters']=Contactfooter::all();
        $data['offres']=DB::table('offres')->where('statut','=',0)->get();
        $data['toffres']=DB::table('toffres')->get();
        return view('frontend.offres',$data);
    }
    
  


    public function faq(){
        $data['menu']='faq';
        $data['socials']=Social::all();
        $data['flags']=Flag::all();
        $data['ibpfooters']=Ibpfooter::all();
        $data['contactfooters']=Contactfooter::all();
       return view('frontend.faq',$data);
    }

}
 