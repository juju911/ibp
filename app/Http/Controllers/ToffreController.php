<?php

namespace App\Http\Controllers;

use App\Toffre;
use App\Presentation;
use DOMDocument;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

class ToffreController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->AdminAuthCheck();
        $toffres=DB::table('toffres')
            ->get();
       return view('backend.toffre-liste',compact('toffres'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->AdminAuthCheck();
        return view('backend.toffre-add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $title=$request->title;
        $description=$request->description;
        $title_en=$request->titleen;
        $description_en=$request->descriptionen;

        $toffre = new Toffre();
        $toffre->title = $title;
        $toffre->description = $description;
        $toffre->title_en = $title_en;
        $toffre->description_en = $description_en;

        $toffre->save();
        return redirect()->route('toffre.index')->with(['message' => ' crée avec succes']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Toffre  $toffre
     * @return \Illuminate\Http\Response
     */
    public function show(Toffre $toffre)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Toffre  $toffre
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['toffre'] = Toffre::find($id);
        return view('backend.toffre-edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Toffre  $toffre
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Toffre $toffre)
    {
        $title=$request->title;
        $description=$request->description;
        $title_en=$request->titleen;
        $description_en=$request->descriptionen;

        $toffre = Toffre::find($request->id);
        $toffre->title = $title;
        $toffre->description = $description;
        $toffre->title_en = $title_en;
        $toffre->description_en = $description_en;

        $toffre->update();
        return redirect()->route('toffre.index')->with(['message' => 'modifiée avec succes']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Toffre  $toffre
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Toffre::destroy($id)){
            return redirect()->route('toffre.index')->with(['message' => 'supprimé avec succes']);
        }else{
            return back()->with(['danger' => 'Erreur dans la suppression']);
        }
    }

    public function AdminAuthCheck(){
        $admin_id=Session::get('admin_id');
        if ($admin_id) {
            return;
        }else {
            return Redirect::to('/login')->send();
        }
    }
}
