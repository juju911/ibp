<?php

namespace App\Http\Controllers;

use App\Professional;
use App\Social;
use App\Flag;
use App\Ibpfooter;
use App\Contactfooter;
use DOMDocument;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

class ProfessionalController extends Controller
{
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->AdminAuthCheck();
        $professionals=DB::table('professionals')
            ->get();
       return view('backend.professional-liste',compact('professionals'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->AdminAuthCheck();
        return view('backend.professional-add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $title=$request->title;
        $description=$request->description;
        $title_en=$request->titleen;
        $description_en=$request->descriptionen;

        $professional = new Professional();
        $professional->titre = $title;
        $professional->description = $description;
        $professional->titre_en = $title_en;
        $professional->description_en = $description_en;
        $image = $request->file('image');

        if ($image)
        {
            $filename = $image->getClientOriginalName();
            $ext = strtolower($image->getClientOriginalExtension());
            $image_full_name = $filename ;
            $upload_path = 'images/professional/';
            $slider_image = $upload_path . $image_full_name;
            $success = $image->move($upload_path, $image_full_name);

            if ($success) {
                $professional->image=$slider_image;
                $professional->save();
            }
        }

        return redirect()->route('profesionnal.index')->with(['message' => ' crée avec succes']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Actualite  $actualite
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function show($id)
    {

        $data['professional']=DB::table('professionals')
            ->where('id',$id)
            ->first();
        $data['socials']=Social::all();
        $data['flags']=Flag::all();
        $data['others']=DB::table('actualites')
            ->where('id','!=',$id)
            ->get();
        $data['menu']="service";
        $data['ibpfooters']=Ibpfooter::all();
        $data['contactfooters']=Contactfooter::all();

        return view('frontend.details-professional',$data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Actualite  $actualite
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['professional'] = Professional::find($id);
        return view('backend.professional-edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Actualite  $actualite
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $title=$request->title;
        $description=$request->description;
        $title_en=$request->titleen;
        $description_en=$request->descriptionen;

        $professional = Professional::find($request->id);
        $professional->titre = $title;
        $professional->description = $description;
        $professional->titre_en = $title_en;
        $professional->description_en = $description_en;
        $image = $request->file('image');

        if ($image)
        {
            $filename = $image->getClientOriginalName();
            $ext = strtolower($image->getClientOriginalExtension());
            $image_full_name = $filename ;
            $upload_path = 'images/professional/';
            $slider_image = $upload_path . $image_full_name;
            $success = $image->move($upload_path, $image_full_name);

            if ($success) {
                $professional->image=$slider_image;
            }
        }
        $professional->update();
        return redirect()->route('profesionnal.index')->with(['message' => 'Actualité modifiée avec succes']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Actualite  $actualite
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Professional::destroy($id)){
            return redirect()->route('profesionnal.index')->with(['message' => 'Actualité supprimé avec succes']);
        }else{
            return back()->with(['danger' => 'Erreur dans la suppression']);
        }
    }
    public function AdminAuthCheck(){
        $admin_id=Session::get('admin_id');
        if ($admin_id) {
            return;
        }else {
            return Redirect::to('/login')->send();
        }
    }
}


